export TERMINAL=kitty
export CM_LAUNCHER=rofi
export PATH="$PATH:$HOME/.local/bin"
export RANGER_DEVICONS_SEPARATOR=" "
export BROWSER=brave

# uwsm for Hyprland
export UWSM_USE_SESSION_SLICE=true
export UWSM_APP_UNIT_TYPE=service

# icon & cursor themes
if [ -d "$HOME/.local/share/icons/" ]; then 
  export XCURSOR_PATH="${XCURSOR_PATH}:$HOME/.local/share/icons"
fi 

# cuda
if [ -d "/opt/cuda" ]; then
  export CUDA_HOME=/opt/cuda
  export PATH=${CUDA_HOME}/bin:${PATH}
  export LD_LIBRARY_PATH=${CUDA_HOME}/lib64:$LD_LIBRARY_PATH
fi

# Where AppImages live.
if [ -d "$HOME/Applications/" ]; then 
  PATH="$PATH:$HOME/Applications"
fi

# Path for my personal scripts
if [ -d "$HOME/.scripts/" ]; then 
  PATH="$PATH:$HOME/.scripts"
fi

if [ -d "$HOME/.cargo/bin/" ]; then 
  PATH="$PATH:$HOME/.cargo/bin/"
fi

# Path for Rofi scripts
if [ -d "$HOME/.config/rofi/bin/" ]; then 
  PATH="$PATH:$HOME/.config/rofi/bin"
fi

# Path & variables for DOOM Emacs - otherwise use Sublime Text & neovim
if [ -d "$HOME/.config/emacs/bin/" ]; then 
  PATH="$PATH:$HOME/.config/emacs/bin/"
  #export CODEEDITOR="emacsclient -c -a emacs"
  #export EDITOR="emacsclient -t"
  export ALTERNATE_EDITOR=nvim
  export CODEEDITOR=nvim
  export EDITOR=nvim
  export SUDO_EDITOR="emacsclient -nw"  
  export VISUAL="emacsclient -c -a emacs"
else
  export ALTERNATE_EDITOR=subl
  export CODEEDITOR=nvim
  export EDITOR=nvim
  export SUDO_EDITOR=nvim
  export VISUAL=subl
fi
