![img](./.images/rice_preview_01.png)

![img](./.images/rice_preview_02.png)

![img](./.images/rice_preview_03.png)

# About
This is a collection of my [dotfiles](https://wiki.archlinux.org/title/Dotfiles). While this repository was initially created for me to quickly deploy my own window manager environments - I have decided to make it public, as reference for those interested in creating their own setup.

# Disclaimer
The commands listed below work on my system and my specific hardware. I make no claims that you should use my deployment method on your personal hardware. Ensure you **perform a full system backup** before following any guides online. I take no responsibility for any damage you do to your system or loss of your personal data.

This repository is constantly advancing. Everything changes! Please don't rely on this project being a base for your own default setup. Files and folders may be added and removed without warning.

# Why is there no installer?

My dotfiles are recommended for users who have experience with Linux and who are comfortable locating, reading, understanding and editing config files. I don't recommend anyone use my dotfiles "as is". Learn to configure your environment yourself. Not only will this prevent you from asking questions on reddit or Discord on how to fix things in some random guys personal configs online - you'll learn so much on how your setup should work. This is why there will never be a ```./install.sh``` for my dotfiles. You learn by doing, not by copy/pasting.

# Overview

| Operating System: | [Arch Linux](https://archlinux.org/download/) | Hotkeys |
| :--- | --- | --- |
| Wayland Compositor: | [Hyprland](https://wiki.hyprland.org/) + [uwsm](https://wiki.hyprland.org/Useful-Utilities/Systemd-start/) | [All](https://gitlab.com/supashang/dotfiles/-/blob/master/.config/hypr/binds/binds-uwsm.conf?ref_type=heads)    |
| Workspaces:   | 9 (per-monitor, using [hypr-ws-switch.sh](https://gitlab.com/supashang/dotfiles/-/blob/master/.scripts/hypr-ws-switch.sh?ref_type=heads)) on Hyprland | `Super + 1-9` |
| Shell:        | [Zsh](https://www.zsh.org/) |     |
| Prompt:       | [starship](https://starship.rs/) |     |
| Status Bar:   | [Waybar](https://github.com/Alexays/Waybar/) |     |
| GTK Theme:    | [Dracula](https://github.com/dracula/gtk), [catppuccin](https://github.com/catppuccin/catppuccin) |     |
| Colors:       | [fzf-wal](https://github.com/surskitt/fzf_wal), [pywal](https://github.com/dylanaraps/pywal/), [wallust](https://codeberg.org/explosion-mental/wallust) |     |
| Mouse Cursor: | [Bibata](https://github.com/rtgiskard/bibata_cursor) |     |
| Plugins:      | [Pyprland](https://github.com/hyprland-community/pyprland) |     |
|               | scratchpad | `R_Ctrl + End` |
|               | zoom toggle | `Super + Shift + z` |
|               | zoom in | `Super + Alt + z` |
| Notifications:| [swaync](https://github.com/ErikReider/SwayNotificationCenteri) |     |
| Launcher:     | [Rofi](https://github.com/davatorium/rofi) | `Super + Space` |
| Terminal:     | [Kitty](https://github.com/kovidgoyal/kitty) (main) | `Super + Return` |
|               | [foot](https://codeberg.org/dnkl/foot) (optional) | `Super + Alt + Return` |
| Quick Menu:   | [PumaMenu](https://gitlab.com/supashang/dotfiles/-/blob/master/.scripts/pumamenu.json?ref_type=heads) | `Super + z` |
| File Managers:| [Thunar](https://docs.xfce.org/xfce/thunar/start) | `Super + t` |
|               | [yazi](https://github.com/sxyazi/yazi) | `R_Ctrl + End` |
| AI:           | [ollama](https://ollama.com/) (local) | `Super + l` |
|               | [stable-diffusion-webui-forge](https://github.com/lllyasviel/stable-diffusion-webui-forge) | `Super + Alt + a` |
| Browsers:     | [Brave](https://brave.com/) | `Super + q` |
|               | [zen](https://zen-browser.app/) | `Super + Alt + q` |
| Email:        | [Thunderbird](https://www.thunderbird.net/en-GB/) | `Super + m` |
| Text Editors: | [neovim](https://neovim.io/) | | 
|               | [DOOM Emacs](https://github.com/doomemacs/doomemacs) | `Super + e` `e` |
|               | [Sublime Text](https://www.sublimetext.com/download) | `Super + e` `s` |
|               | [VSCodium](https://github.com/VSCodium/vscodium)     | `Super + e` `c` |
| Gaming:       | [Steam](https://store.steampowered.com/)  | `Super + g` `s`    |
|               | [Heroic Games Launcher](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/) | `Super + g` `h` |
|               | [Lutris](https://lutris.net/) | `Super + g` `l` |
|               | [Game mode](https://gitlab.com/supashang/dotfiles/-/blob/master/.scripts/hypr-gamemode.sh?ref_type=heads) | `Super + Shift + g`|
| Audio:        | [mpd](https://www.musicpd.org/) | |
|               | [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp) | `Super + a` |
|               | [pulsemixer](https://github.com/GeorgeFilipkin/pulsemixer) | `Super + v` |
| Media:        | [YACReader](https://www.yacreader.com/) | `Super + y` | 
|               | [Audacity](https://www.audacityteam.org/) | |
|               | [Calibre](https://calibre-ebook.com/) | |
|               | [Gimp](https://www.gimp.org/) | `Super + i` `g` |
|               | [Blender](https://www.blender.org/) | |
|               | [Krita](https://krita.org/en/) | `Super + i` `k` |
| Social:       | [Mumble](https://www.mumble.info/) | `Super + Shift + m` |  
|               | [Telegram](https://telegram.org/)  | `Super + Shift + t` | 
|               | [Signal](https://signal.org/) |     |
| Sysmon:       | [btop](https://github.com/aristocratos/btop) | `Super + Alt + b` |
|               | [nvtop](https://github.com/Syllo/nvtop) | `Super + Alt + n` |
| Wallpaper:    | [Random](https://gitlab.com/supashang/dotfiles/-/blob/master/.scripts/wallpaper_random.sh?ref_type=heads) | `Super + p` |
|               | [Set theme folder](https://gitlab.com/supashang/dotfiles/-/blob/master/.scripts/wallpaper_folder_path.sh?ref_type=heads) | `Super + w` |
|               | [Switch wallpaper tool](https://gitlab.com/supashang/dotfiles/-/blob/master/.scripts/wallpaper_setter.sh?ref_type=heads) | `Super + Shift + w` |
|               | [Variety](https://peterlevi.com/variety/) | |
|               | [swww](https://github.com/LGFae/swww) | |
|               | [wpaperd](https://github.com/danyspin97/wpaperd) | |
| Screenshot:   | "Full", "Window", "Area", "Dropbox shared", "Video" | `PrtScn` |

# Hardware
These dotfiles are used with an Intel CPU and NVIDIA RTX 4070 GPU.

# Getting Started
- Create a bootable USB drive with [Arch Linux](https://archlinux.org/download/) on it. I recommend using [Ventoy](https://www.ventoy.net/en/index.html) for this.
- Reboot and select to boot from the USB via your BIOS options.
- When in the Arch Linux shell prompt do: `setfont -d && pacman-key --refresh-keys`
- [Install Arch Linux](https://wiki.archlinux.org/title/Installation_guide): `archinstall`
- Choose Bootloader: GRUB
- Choose File system: ext4
- Display Manager: sddm
- Select Desktop profiles: Hyprland
- Enable [multilib](https://wiki.archlinux.org/title/Official_repositories#multilib)
- Install additional packages: git, neovim, thunar, meld, kitty, xterm, firefox, rofi, rsync,
- Audio server: pipewire
- Use Network Manager

When asked to chroot after installation - select: `Yes`

then:

# NVIDIA Users
Hyprland does not officially support NVIDIA hardware. However, it can work with a little tinkering.
Please [read the Hyprland wiki](https://wiki.hyprland.org/Nvidia/#drm-kernel-mode-setting) for configuration and check [Hyprland Git](https://github.com/hyprwm/Hyprland/issues) regarding any issues.
These steps may not be needed on later NVIDIA driver versions, so please check compatibility with your GPU.

Create and edit `/etc/modprobe.d/nvidia.conf`:

```sh
sudo nvim /etc/modprobe.d/nvidia.conf
```
Press `i` to enter **INSERT** mode, and add the following:

```sh
options nvidia_drm modeset=1 fbdev=1
```
Press `Esc` to enter **NORMAL** mode, then save and exit nvim: `:wq`

Edit the `/etc/mkinitcpio.conf` file:

```sh
sudo nvim /etc/mkinitcpio.conf
```
add the following to `MODULES()`:

```config
MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)
```
Save and exit nvim: `:wq`

Rebuild initramfs:

```sh
sudo mkinitcpio -P
```
Exit chroot: `exit`

Reboot and remove USB drive.

# Login to Hyprland.
## Default Keybinds
Open a terminal: `Super + q`
Close a window: `Super + c`

Open Firefox to [my dotfiles rep](https://gitlab.com/supashang/dotfiles) to continue the installation:

# Installation
## Chaotic AUR
In your terminal enter:
```sh
sudo pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
sudo pacman-key --lsign-key 3056513887B78AEB
sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst'
sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
```
Add the [Chaotic AUR](https://aur.chaotic.cx/docs) to your `/etc/pacman.conf`
```config
sudo nvim /etc/pacman.conf
```
add the following:
```config
[chaotic-aur]
Include = /etc/pacman.d/chaotic-mirrorlist
```
Save the file with `:wq`

Then in your terminal do:
```sh
sudo pacman -Sy
```

## Shell Configuration
### Zsh
I use Zsh with my configuration. To install it, and the plugins I use, open a terminal and enter:

```sh
sudo pacman -S zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

When prompted, change your user shell to Zsh.

### Zsh Plugins
```sh
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

## Shell Prompt
### Starship
Ensure that the [starship](https://starship.rs/installing/) prompt binary is installed.
```sh
sudo pacman -S starship
```
Add this to your `.zshrc` file:
```sh
eval "$(starship init zsh)"
export STARSHIP_CONFIG=~/.config/starship/starship.toml
```
# pacman.conf
I made the following edits to my [pacman.conf](https://man.archlinux.org/man/pacman.conf.5) to speed up installing packages.

`ParallelDownloads` - Specifies number of concurrent download streams. The value needs to be a positive integer. If this config option is not set then only one download stream is used (i.e. downloads happen sequentially).
`Color` - Automatically enable colors only when pacman’s output is on a tty.
`ILoveCandy` - animates the update progress bar.

```sh
sudoedit /etc/pacman.conf
```

**Misc options** section:

```config
# Misc options
#UseSyslog
ILoveCandy
Color
#NoProgressBar
CheckSpace
VerbosePkgLists
ParallelDownloads = 10
DownloadUser = alpm
#DisableSandbox
```

# AUR Helpers
Many of the apps I use are located in the [AUR](https://aur.archlinux.org/) & [Chaotic AUR](https://aur.chaotic.cx/docs), therefore, you **will need to have those enabled** before continuing.

If you have the [Chaotic AUR](https://aur.chaotic.cx/docs) enabled in your configuration, you can quickly install `yay` or `paru`:

```sh
sudo pacman -S yay paru
```

## Manually installing [paru](https://github.com/Morganamilo/paru)
```sh
# check if paru is installed
which paru
```

If not installed:
```sh
sudo pacman -S --needed base-devel && git clone https://aur.archlinux.org/paru.git && cd paru && makepkg -si
```

## Manually installing [yay](https://github.com/Jguer/yay)
```sh
# check if yay is installed
which yay
```
If not installed:
```sh
sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si
```

# Install Apps
All the apps I use have been saved to a `packages.txt` file. These are what will be installed by default by following this guide.
Have a look through the `packages.txt` file and remove any apps **you do not want** to use on your system or that may have issues building from source.

## AMD Users
If you are a `AMD CPU/GPU` user, you will need to **remove** anything to do with `NVIDIA / cuda / intel` from my `packages.txt` file before continuing.
```sh
nvim packages.txt
```
Save and exit with `:wq`

## Fish Shell Users
You may need to switch to either Bash or Zsh to continue. You can see which shell you are using by typing:
```sh
echo $SHELL
```

## Bulk Install Packages 
You can install all the apps listed in the `packages.txt` file by **opening a terminal in the folder where the packages.txt file is located** and entering the following command in a terminal:
```sh
yay -S --needed $(comm -12 <(yay -Slq | sort) <(sort packages.txt))
```
This will take a while, so go grab a coffee! ☕

## supadots
Create a folder to download my dotfiles to:

```sh
mkdir supadots/
cd supadots/
git clone https://gitlab.com/supashang/dotfiles.git
cd dotfiles/
```
- **Create a backup of your existing config files**.
- Move the files and folders from `~/supadots/dotfiles` into your `$HOME` folder using Thunar - ignore the `.git` folder, as it is not needed.

## Vim
I'm using the the [NvChad](https://nvchad.com/docs/quickstart/install) configs and workflow:
```sh
git clone https://github.com/NvChad/starter ~/.config/nvim && nvim
```
Some basic NvChad keys/commands:

| Action                   | Command               | Hotkeys                  |
| :---                     | ---                   | ---                      |
| Insert Mode              |                       | i                        |
| Normal Mode              |                       | Esc                      |
| Write To File            | :w                    |                          |
| Exit                     | :q                    |                          |
| Quit Window              |                       | `Ctrl + w` `q`           |
| Find Files               |                       | `Space + f f`            |
| Switch Buffers           |                       | `Tab`                    | 
| Search Forward           |                       | /                        |
| Search Backward          |                       | ?                        |
| Vertical Split           | :vsp                  | `Ctrl + w` `v`           |
| Horizontal Split         | :sp                   | `Ctrl + w` `s`           |
| Split Navigation         |                       | `Ctrl +` `h` `j` `k` `l` |
| Cheatsheet Help          |                       | `Space + c h`            |  
| Toggle Line numbers      |                       | `Space + n`              |
| Horizontal Terminal      |                       | `Space + h`              |
| Vertical Terminal        |                       | `Space + v`              |
| Switch Themes            |                       | `Space + t` `h`          |
| [NvimTree](https://github.com/nvim-tree/nvim-tree.lua/blob/master/doc/nvim-tree-lua.txt#L155)                 | :NvimTreeToggle       | `Ctrl + n` (toggles)     |
| NvimTree - Find file     | :NvimTreeFindFile     | `f`                      |
| Nvimtree - Create file   |                       | `a`                      |
| NvimTree - Mark file     |                       | `m`                      |
| NvimTree - Rename file   |                       | `r`                      |
| NvimTree - Copy file     |                       | `c`                      |
| NvimTree - Paste file    |                       | `p`                      |   
| NvimTree - Delete file   |                       | `d`                      |

## Emacs
You can install the `emacs-nativecomp` package as follows:
```sh 
yay -S emacs-nativecomp
```
**IMPORTANT:** To avoid any conflicts when installing DOOM Emacs - ensure to remove the `~/.emacs.d` folder if on BEFORE continuing.

### [DOOM Emacs](https://github.com/doomemacs/doomemacs)

"Doom is a configuration framework for GNU Emacs"
I use [DOOM Emacs](https://github.com/doomemacs/doomemacs) as my Emacs environment.

1.  DOOM EMACS DEPENDENCIES

    These are the things required for my DOOM Emacs config to work correctly.
    
    [ripgrep](https://github.com/BurntSushi/ripgrep)
    ```sh 
    yay -S ripgrep
    ```    
    
    [isort](https://pypi.org/project/isort/)
    ```sh
    pipx install isort
    ```
    
    Create Org folders.
    ```sh 
    cd
    mkdir -p Org/roam Org/journal
    ```
### Installing DOOM Emacs
```sh
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.config/emacs
~/.config/emacs/bin/doom install
```
I recommend rebooting now.

### DOOM Emacs missing fonts
After DOOM Emacs is installed, launch it and press `L_Alt + x` and search for `nerd-icons-install-fonts` and select it to install the missing fonts.

Finally, to check everything is installed - reboot and open a terminal and type:
```sh
doom doctor
```
# File Edits
My dotfiles are for my system, if you are using them on your own system, there are a number of entries in config files that will need edits:

- Edit you keyboard layout in: `~/.config/hypr/input/input.conf`
- Edit your town name after `--location` in the Waybar **// Weather** widget: `~/.config/waybar/hyprland-config`
- Choose the name of a monitor file which best fits your display hardware in: `~/.config/hypr/workspaces/` and source it in: `~/.config/hypr/hyprland.conf`
- Use `nwg-displays` to set your monitor settings. These are saved to: `~/.config/hypr/monitors.conf`
- Use `nwg-look` to set your gtk themes.
- Use `qt6ct` and `Kvantum Manager` to set your qt themes.
- Edit the file `~/.config/hypr/pyprland.toml` and set the monitor ID that you want your scratchpads to appear on after `force_monitor =`
- Edit your monitor variables to open certain apps on certain monitors in: `~/.config/hypr/windowrules/windowrule.conf`
- Edit your ```$HOME``` backup destination paths in: ```~/.scripts/home_backup.sh```
- Edit your **media** backup destination paths in: ```~/.scripts/media_backup.sh``` 

# Wallpapers
## Create Wallpaper and Screenshots Folders
```sh 
cd
mkdir -p Pictures/wallpaper/ Pictures/screenshots/ Pictures/lockscreen
```
I have my wallpaper folders separated as theme sub-folders in the `$HOME/Pictures/wallpaper` folder.

Example:
``` example
    $HOME/Pictures/
         ├──wallpaper/
            ├── abstract/
            ├── animals/
            ├── artistic/
            ├── cars/
            ├── comics/
            ├── gruvbox/
            ├── neon/
            ├── purple/

```
## Search & Download Wallpapers
To search for wallpapers from [wallhaven.cc](https://wallhaven.cc/), bind [this script](https://gitlab.com/supashang/dotfiles/-/blob/master/.scripts/wallpaper_downloader.sh) to a keybind or use the **PumaMenu** (`super + z`) **Wallpaper - Downloader** option.
It uses `nsxiv` to display thumbnails of the wallpapers you searched for. You can mark images to download by pressing (`m`) and then (`q`) to close nsxiv and download the images to the `$HOME/Pictures/wallpaper/wallhaven` folder.

## Set Wallpaper
To set an image folder as the current theme - select **Wallpaper - Set Theme Folder** with PumaMenu (`super + z)`. The script will select a random picture from the chosen theme folder and display it. You can now cycle through random wallpapers from the chosen theme sub-folder by pressing: `super + shift + p`

To change the software which sets your wallapper, press ```super + shift + w```. There are two options at present: **swww** or **wpaperd**.

- **swww**: allows for animated transitions, GIF files as wallpapers, and you can manually select a specific wallpaper with ```super + p``` 
- **wpaperd**: will automatically cycle random wallpapers from your chosen theme folder every three minutes. It does not support GIFs as wallpapers, nor provide manual wallpaper selection.

# Wallpapers
- [Wallpaper-Clan](https://wallpapers-clan.com/desktop-wallpapers/)
- [Wallhaven](https://wallhaven.cc/)

# Terminal colour themes
In order to set terminal colours using fzf-wal, you first have to create the **wal cache files**. To do this, in a terminal, use pywal to load a picture from your wallpaper folder. You only need to do this once.
```sh
wal -i Pictures/wallpaper/my_wallpaper_theme_folder/some_random_picture.jpg
```
## [fzf-wal](https://github.com/surskitt/fzf_wal)

Manually select from a number of colour schemes for your terminal.
```sh
pipx install fzf-wal
```
To use it - in a terminal do:
```sh
fzf-wal
```
Your terminal colours will change to adapt to the colours in your selected wallpaper.
- To select a terminal colour with **PumaMenu:** press, `(Super + z)` > `Terminal - Set Colour Theme`

# Hotkeys
## Hyprland: Hotkeys
To view the key binds for Hyprland look at [this configuration file](https://gitlab.com/supashang/dotfiles/-/blob/master/.config/hypr/binds/binds-uwsm.conf?ref_type=heads).

# Post Install Configuration
## Hyprland: Set monitor config

Use `nwg-displays` to set your monitor display settings. Once applied, the config is automatically used by the path set under **MONITORS** in my `hyprland.conf`

## Hyprland: Workspaces
Ensure to edit the [~/.config/hypr/workspaces/workspace-3-monitor.conf](https://gitlab.com/supashang/dotfiles/-/blob/master/.config/hypr/workspaces/workspace-3-monitor.conf?ref_type=heads) to set your workspaces to reflect your monitor setup.

## Hyprland: Set scratchpad main monitor
Set your main monitor for scratchpads using the `force_monitor` variable in [pyprland.toml](https://gitlab.com/supashang/dotfiles/-/blob/master/.config/hypr/pyprland.toml?ref_type=heads#L24)

e.g, DP-1, HDMI-A-1, DP-2, DP-3 &#x2026;

You can obtain your monitor ID by opening a terminal and entering:
```sh
hyprctl monitors
```
## Ranger Icons
In order to have the icons show in the Ranger terminal file manager:

```sh
mkdir $HOME/.config/ranger/plugins
cp /usr/share/ranger/plugins/ranger_devicons/*.py "$HOME/.config/ranger/plugins"
```
## Yazi Plugins
- [smart-enter.yazi](https://github.com/yazi-rs/plugins/tree/main/smart-enter.yazi)
```sh
ya pack -a yazi-rs/plugins:smart-enter
```
- [full-border.yazi](https://github.com/yazi-rs/plugins/tree/main/full-border.yazi)

``` sh
ya pack -a yazi-rs/plugins:full-border
```
- [max-preview.yazi](https://github.com/yazi-rs/plugins/tree/main/max-preview.yazi)

``` sh
ya pack -a yazi-rs/plugins:max-preview
```

## GRUB Theme
To set a new GRUB theme - follow [this guide](https://spacebums.co.uk/post/grub-themes/).

## SDDM Theme
To replace the default SDDM theme - follow [this guide](https://spacebums.co.uk/post/sddm-themes/) or [https://github.com/Keyitdev/sddm-astronaut-theme](https://github.com/Keyitdev/sddm-astronaut-theme)

## Cursor Themes
This is an ongoing battle! Getting cursor themes to work across different GUI frameworks feels more like a Linux problem than a Hyprland problem. Cursors appear to change in size, not work at all on some apps, appear to be inconsistent, to name a few problems. I recommend **not installing cursor themes with your package manager**, but rather, downloading manually and copy them to ```~/.local/share/icons/``` and maintain them yourself. A user created a project called [accurse](https://github.com/ATM-Jahid/accurse) to convert cursors themes for use with Hyprland for this very purpose.   

## Timeshift
To enable automatic Timeshift snapshots, open a terminal and enter:
```sh
sudo systemctl enable cronie.service
```
Then, launch Timeshift to set up your backup location.

To launch Timeshift with Hyprland - enter this into the terminal:
```sh
sudo -E timeshift-launcher
```
# Ollama
## DOOM Emacs + Ellama

To use AI features in Emacs.
```sh
yay -S ollama-cuda
systemctl enable ollama.service
systemctl start ollama.service
ollama pull llama3.2
```
In Emacs do `ALT + x` and search for `ellama chat`

## Ollama scratchpad
You can use ollama (AI chat) in the terminal as a drop-down scratchpad. First, ensure that the `ollama.service` is active and enabled. Then ensure that you have a chat model installed:

```sh
yay -S ollama-cuda
systemctl enable ollama.service
systemctl start ollama.service
ollama pull llama3.2
```
The scratchpad is set in the `pyprland.conf` located in `~/.config/hypr/`
The hotkey is currently set in [~/.config/hypr/binds/binds-uwsm.conf](https://gitlab.com/supashang/dotfiles/-/blob/master/.config/hypr/binds/binds-uwsm.conf?ref_type=heads#L69) as `Super + l`

# Pyprland
If you encounter any issues with pyprland not working as intended. Please consult the online documentation regarding installation:
[https://hyprland-community.github.io/pyprland/](https://hyprland-community.github.io/pyprland/)

# Gaming
In order to solve most gaming related issues when using Hyprland, use [gamescope](https://github.com/ValveSoftware/gamescope).
Here is an example of running [No Man's Sky](https://www.protondb.com/app/275850) with Steam + Proton using gamescope:
```example
gamescope -W 2560 -H 1440 -r 240 --force-grab-cursor --adaptive-sync -- %command%
```
## Miniconda
To manage the many Python environments and dependencies that may conflict with the latest operating system install of Python, I use [Miniconda](https://docs.conda.io/en/latest/miniconda.html#installing). To install it, download the **Miniconda3 Linux 64-bit** script and execute it in the terminal.

```sh
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
    
~/miniconda3/bin/conda init bash
~/miniconda3/bin/conda init zsh
```
Disable default conda `base` environment:
```sh
conda config --set auto_activate_base false
```
An [example](https://spacebums.co.uk/oobabooga-chat/) of how and why I use Miniconda with git projects can be found on my website.

# Maintenance
To see how I set up and maintain this dotfiles repository visit [my guide here.](https://spacebums.co.uk/dotfiles-git-bare-repo/)

# License
The MIT License (MIT)

Copyright (c) 2021

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Special Thanks
A special thanks to those whose work and contributions made this possible:

- [Hyprland](https://hyprland.org/)
- [mylinuxforwork](https://github.com/mylinuxforwork)
- [Arch Wiki](https://wiki.archlinux.org/title/Hyprland)
- [r/hyprland](https://www.reddit.com/r/hyprland/)
- [awesomewm](https://awesomewm.org/)
- [r/awesomewm](https://www.reddit.com/r/awesomewm/)

And everyone else I may have missed.


