#!/bin/env bash

conkyRC="$HOME/.conky/MX-Asq/MX-GeekyTowerLogo/MX-geekytowerLogo"

# Try to get a list of PIDs for appropriate conky's
runningConkys=$(pgrep -a conky | awk '$conkyRC/{print $1}')

# if runningConkys is empty
if [[ -z "$runningConkys" ]]; then
    # start conky
    conky -q -c "$conkyRC" &
else
    #Kill all of the PIDs listed in $runningConkys
    echo "$runningConkys" | xargs -n 1 kill -15
fi
