#!/bin/env bash
#  _   _                        ____             _
# | | | | ___  _ __ ___   ___  | __ )  __ _  ___| | ___   _ _ __
# | |_| |/ _ \| '_ ` _ \ / _ \ |  _ \ / _` |/ __| |/ / | | | '_ \
# |  _  | (_) | | | | | |  __/ | |_) | (_| | (__|   <| |_| | |_) |
# |_| |_|\___/|_| |_| |_|\___| |____/ \__,_|\___|_|\_\\__,_| .__/
#                                                          |_|
# Description:
# Run rsync to backup $HOME to a destination.
# Folders listed in $HOME/.rsync-exclude.txt will be ignored.
#
# Dependencies: rsync, gum, figlet
#
#
set -euo pipefail

# Edit the line below to the path of your backup destination folder.
backup_dest_1='/run/media/supa/Fat_Boy/.backups/Home_Backup/'
# Secondary backup location
backup_dest_2='/run/media/supa/WDRed/home_backup/'

if command -v figlet
then
    figlet "Home Backup"
fi

echo ":: Would you like to perform a backup of your $HOME folder?"
choice=$(gum choose "Yes" "No")
if [ "$choice" == "No" ]; then
    exit
else
    echo "Choose your backup destination:"
    bk_choice=$(gum choose "$backup_dest_1" "$backup_dest_2")

    # Does the backup folder exist?
    if [ ! -d "$bk_choice" ]; then
        errormsg="ERROR: Backup destination not found: $bk_choice"
        notify-send "$errormsg"
        echo "$errormsg"
        exit 2
    else
        # Backup.
        cd "$HOME/" || exit 2
        backup_msg="Performing backup of $HOME folder to $bk_choice"
        notify-send "$backup_msg"
        echo "$backup_msg"
        sleep 1
        rsync -avz --stats --progress --delete-excluded --exclude-from='.rsync-exclude.txt' "$HOME/" "$bk_choice"

        echo ":: BACKUP COMPLETE."

        # Allow user to review backup log before exiting.
        exit_msg=$(gum choose "Exit")
        if [ "$exit_msg" == "Exit" ]; then
            exit
        fi
    fi
fi
