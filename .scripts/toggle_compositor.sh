#!/bin/env bash
#
# Enable/disable compositor
if [ "$DESKTOP_SESSION" == "awesome" ]; then
    ppid="picom"
    if pgrep -x "$ppid" >/dev/null
    then
        kill "$(pgrep -x "$ppid")"
    else
        picom &
    fi
fi
