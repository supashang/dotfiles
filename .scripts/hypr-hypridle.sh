#!/bin/env bash

if [ -n "$(pidof hypridle)" ]; then 
  killall hypridle 
  echo "Hypridle disabled."
else 
  hypridle 2> /dev/null 
  echo "hypridle enabled."
fi
