#!/bin/env bash
#  __  __          _ _         ____             _
# |  \/  | ___  __| (_) __ _  | __ )  __ _  ___| | ___   _ _ __
# | |\/| |/ _ \/ _` | |/ _` | |  _ \ / _` |/ __| |/ / | | | '_ \
# | |  | |  __/ (_| | | (_| | | |_) | (_| | (__|   <| |_| | |_) |
# |_|  |_|\___|\__,_|_|\__,_| |____/ \__,_|\___|_|\_\\__,_| .__/
#                                                         |_|
# Description:
# Run rsync to backup media files to a destination.
#
# Dependencies: rsync, gum, figlet

set -euo pipefail

# Edit the line below to the path of your backup destination folder.
backup_dest_1='/run/media/supa/Fat_Boy/'
# Secondary backup location
backup_dest_2='/run/media/supa/WDRed/'

# The folder choices to backup. Exclude trailing / to copy the folder plus its contents to the desination.
pics="$HOME/Pictures"
music="$HOME/Music"
vids="$HOME/Videos"
work="$HOME/Work"

if command -v figlet
then
    figlet "Media Backup"
fi

echo ":: Would you like to perform a backup of your media files folder?"
choice=$(gum choose "Yes" "No")
if [ "$choice" == "No" ]; then
    exit
else
    echo "Choose your backup destination:"
    bk_choice=$(gum choose "$backup_dest_1" "$backup_dest_2")

    # Does the backup folder exist?
    if [ ! -d "$bk_choice" ]; then
        errormsg="ERROR: Backup destination not found: $bk_choice"
        notify-send "$errormsg"
        echo "$errormsg"
        exit 2
    else
        echo "Choose which folder you'd like to backup..."
        sync_choice=$(gum choose "$pics" "$music" "$vids" "$work")

        if [ ! -e "$sync_choice" ]; then
            echo "Error: $sync_choice not found!"
            exit
        else
            cd "$HOME/" || exit 2
            backup_msg="Performing backup of $sync_choice folder to $bk_choice"
            notify-send "$backup_msg"
            gum spin --spinner dot --title "$backup_msg" -- sleep 1
            rsync -Lavz --delete --stats --progress "$sync_choice" "$bk_choice"
            echo ":: BACKUP COMPLETE."
        fi

        # Allow user to review backup log before exiting.
        exit_msg=$(gum choose "Exit")
        if [ "$exit_msg" == "Exit" ]; then
            exit
        fi
    fi
fi
