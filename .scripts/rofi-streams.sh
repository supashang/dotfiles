#!/bin/env bash
#
#  ▓▓▓▓▓▓▓▓▓▓
# ░▓ author ▓ cirrus <cirrus@archlinux.info>
# ░▓ code   ▓ https://gist.github.com/cirrusUK
# ░▓ mirror ▓ http://cirrus.turtil.net
# ░▓▓▓▓▓▓▓▓▓▓
# ░░░░░░░░░░
#
# Modified by Supa Shang
# Removed broken urls.
# Added new working urls

#set -euo pipefail

declare -A LABELS
declare -A COMMANDS

#Plan 9
COMMANDS["Plan9"]='mpv "https://youtu.be/Ln7WF78PolA"'
LABELS["Plan9"]=""

# Snow
COMMANDS["Snowing"]='mpv "https://youtu.be/CQSfI_tXZLY"'
LABELS["Snowing"]=""

# Underwater
COMMANDS["Underwater"]='mpv "https://youtu.be/843Rpqza_6o"'
LABELS["Underwater"]=""

# Coastlines
COMMANDS["Coastlines"]='mpv "https://youtu.be/CbdJYCYAgtk"'
LABELS["Coastlines"]=""

# Sky News
COMMANDS["Sky_News"]='mpv "https://www.youtube.com/watch?v=YDvsBbKfLPA"'
LABELS["Sky_News"]=""

# BBC News
COMMANDS["BBC_News"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=27" high'
LABELS["BBC_News"]=""

# BBC1
COMMANDS["BBC1"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=2" high'
LABELS["BBC1"]=""

# BBC2
COMMANDS["BBC2"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=25" high'
LABELS["BBC2"]=""

# KUNG FU CLASSICS
COMMANDS["KUNG_FU_CLASSICS"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=1967" high'
LABELS["KUNG_FU_CLASSICS"]=""

# UFC
COMMANDS["UFC"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=713" high'
LABELS["UFC"]=""

# The Beverly Hillbillies
COMMANDS["The_Beverly_Hillbillies"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=2969" high'
LABELS["The_Beverly_Hillbillies"]=""

# Nat Geo
COMMANDS["National_Geographic"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=6485" high'
LABELS["National_Geographic"]=""

# Channel 4
COMMANDS["Channel4"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=2" high'
LABELS["Channel4"]=""

# Westerns
COMMANDS["Westerns"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=5582" high'
LABELS["Westerns"]=""

# FuryTV
COMMANDS["FuryTV"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=4628" high'
LABELS["FuryTV"]=""

# Research
COMMANDS["Research"]='streamlink -p mpv -a \"--no-audio\" "https://www.filmon.com/tv/channel/export?channel_id=2819" high'
LABELS["Research"]=""

# Cheerleaders
COMMANDS["Cheerleaders_NSFW"]='streamlink -p mpv -a \"--no-audio\" "https://www.filmon.com/tv/channel/export?channel_id=2939" high'
LABELS["Cheerleaders_NSFW"]=""
# Bikini TV
COMMANDS["BikiniTV_NSFW"]='streamlink -p mpv -a \"--no-audio\" "https://www.filmon.com/tv/channel/export?channel_id=2000" high'
LABELS["BikiniTV_NSFW"]=""
# Bikini Down Under
COMMANDS["Bikini_Down_Under"]='streamlink -p mpv -a \"--no-audio\" "https://www.filmon.com/tv/channel/export?channel_id=2819" high'
LABELS["Bikini_Down_Under"]=""

# Mad Science
COMMANDS["Mad_Science"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=4604" high'
LABELS["Mad_Science"]=""

# Classics
COMMANDS["Classics"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=3377" high'
LABELS["Classics"]=""

# Really
COMMANDS["Really"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=2798" high'
LABELS["Really"]=""

# B-Movie
COMMANDS["B-Movie"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=3274" high'
LABELS["B-Movie"]=""

# Sci Fi
COMMANDS["SciFi"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=3668" high'
LABELS["SciFi"]=""

# Alien Invasion
COMMANDS["Alien_Invasion"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=3122" high'
LABELS["Alien_Invasion"]=""

# Bonanza
COMMANDS["Bonanza"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=2957" high'
LABELS["Bonanza"]=""

# Flash
COMMANDS["Flash"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=5594" low'
LABELS["Flash"]=""

# Robin Hood
COMMANDS["Robin_Hood"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=2972" low'
LABELS["Robin_Hood"]=""

# E4
COMMANDS["E4"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=65" low'
LABELS["E4"]=""

# DRAMA
COMMANDS["Drama"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=5944" low'
LABELS["Drama"]=""

# Film4
COMMANDS["Film4"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=13" low'
LABELS["Film4"]=""

# ITV
COMMANDS["ITV"]='streamlink -p mpv "https://www.filmon.com/tv/channel/export?channel_id=11" low'
LABELS["ITV"]=""

# Generate menu
function print_menu()
{
    for key in "${!LABELS[@]}"
    do
  echo "$key    ${LABELS}"
     #   echo "$key    ${LABELS[$key]}"
     # my top version just shows the first field in labels row, not two words side by side
    done
}

# Show rofi.
function start()
{
    # print_menu | rofi -dmenu -p "?=>"
    print_menu | rofi -dmenu -mesg ">>>" -i -p " 🎬 📺 Rofi Video Streams  📺 🎬  "
}

# Run it
value="$(start)"

# Split input.
# grab upto first space.
choice=${value%%\ *}
# graph remainder, minus space.
#input=${value:$((${#choice}+1))}

# Cancelled? bail out
if test -z "${choice}"
then
    exit
fi

# Check if choice exists
if test "${COMMANDS[$choice]+isset}"
then
    # Execute the choice
    killall mpv
    #eval echo "Executing: ${COMMANDS[$choice]}"
    eval "${COMMANDS[$choice]}"
else
 eval  "$choice" | rofi
fi
