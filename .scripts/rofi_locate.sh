#!/bin/env bash
#
# Rofi Locate: Search files with Rofi.
xdg-open "$(plocate "$HOME" | rofi -dmenu -threads 0 -theme-str 'window {width: 90%;}' -i -p "plocate")"
