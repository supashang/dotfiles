#!/bin/env bash

# Description: Switch between wallpaper setting software.
# Dependencies: gum, figlet, swww, wpaperd, uwsm
# Creator: Supa Shang

gum style \
  --foreground 212 --border-foreground 212 --border double \
  --align center --width 50 --margin "1 2" --padding "2 4" \
  'WALLPAPER SETTER'

printf "
Choose a wallpaper setter.

swww: - Supports animated GIF.
      - Auto-theme colours with wallust.
      - Choose wallpaper with, Super + p
      - Random wallpaper with, Super + Shift + p
wpaperd:
      - Auto-cycles a random images to each monitor every 3 minutes.
      - No auto-theme colours.
      - Random wallapaper with, Super + Shift + p
"

# List of executables to terminate
apps_to_check=("mpvpaper" "wpaperd" "swww-daemon" "swaybg" "hyprpaper" "variety")

if [ "$XDG_BACKEND" == "wayland" ]; then
wp_choice=$(gum choose "swww-daemon" "wpaperd" "Exit")
if [ "$wp_choice" == "Exit" ]; then
exit
else
  # Kill the apps if they're running
  for app in "${apps_to_check[@]}"; do
    if pgrep -f "$app" > /dev/null; then
      killall -9 "$app"
    fi
  done

  if [ "$wp_choice" == "swww-daemon" ]; then
  uwsm app -- swww-daemon --format xrgb
  current_wallpaper="$HOME/.cache/current_wallpaper"
  if [ -f "$current_wallpaper" ]; then
    sleep 1
    line=$(head -n 1 "$HOME/.cache/current_wallpaper")
    swallust.sh "$line"
  fi
  elif [ "$wp_choice" == "wpaperd" ]; then
  uwsm app -- wpaperd -d
  fi
fi
fi
