#!/bin/env bash
#
# https://github.com/pystardust/waldl
# script to find and download wallpapers from wallhaven
#
# Edited by Supa Shang for Rofi and Pumamenu integration.
# We use the $query variable as a destination folder for the downloaded images.
# Check out the YT link for where I found this cool script here: https://youtu.be/C7n-34bEdF8
#
# Press m to mark images to download then press q
set -euo pipefail

####################
## User variables ##
####################
# the dir where wallpapers are stored
walldir="$HOME/Pictures/wallpaper/wallhaven/"
# the dir used to cache thumbnails
cachedir="$HOME/.cache/wallhaven"
# nsxiv options
nsxiv_args=" -tfpo -z 200" # o is needed for selection
# number of pages to show in search results. Each page contains 24 results
max_pages=4
# sorting : date_added, relevance, random, views, favorites, toplist
sorting=date_added
# quality : large original small
quality=large
# atleast : least res
atleast=1920x1080
# purity SFW=000, Sketchy=010, SFWNSFW=110
purity=000

# the menu command used when no query is provided
sh_menu () {
	rofi -dmenu -l 0 -p "Search Wallpapers"
}

##########################
## getting search query ##
##########################
[ -n "$*" ] && query="$*" || query=$( sh_menu )
[ -z "$query" ] && exit 1
query=$(printf '%s' "$query" | tr ' ' '+' )

######################
## start up commands #
######################
rm -rf "$cachedir"
mkdir -p "$cachedir"

# progress display command
sh_info () {
	printf "%s\n" "$*" >&2
	notify-send "wallhaven" "$*"
}

# dependency checking
dep_ck () {
	for pr; do
		command -v "$pr" >/dev/null 2>&1 || sh_info "command $pr not found, install: $pr" 1
	done
}
dep_ck "nsxiv" "curl" "jq"

# clean up command that would be called when the program exits
clean_up () {
	printf "%s\n" "cleaning up..." >&2
	rm "$datafile"
	rm -r "$cachedir"
}

# data file to store the api information
datafile="/tmp/wald.$$"

# clean up if killed
trap "exit" INT TERM
trap "clean_up" EXIT

##################
## getting data ##
##################

# request the search results for each page
get_results () {
	for page_no in $(seq $max_pages)
	do
		{
			json=$(curl -s -G "https://wallhaven.cc/api/v1/search" \
					-d "q=$1" \
					-d "page=$page_no" \
					-d "categories=100" \
					-d "purity=$purity" \
					-d "atleast=$atleast" \
					-d "sorting=$sorting"
				)
			printf "%s\n" "$json" >> "$datafile"
		} &
		sleep 0.001
	done
	wait
}

# search wallpapers
sh_info "getting data..."
get_results "$query"

# check if data file is empty, if so then exit
[ -s "$datafile" ] || sh_info "no images found" 1

############################
## downloading thumbnails ##
############################

# get a list of thumnails from the data
thumbnails=$( jq -r '.data[]?|.thumbs.'"$quality" < "$datafile")

if [ -z "$thumbnails" ]; then
	notify-send "wallhaven" "no-results found"
	exit 1
fi

# download the thumbnails
sh_info "caching thumbnails..."
for url in $thumbnails
do
		printf "url = %s\n" "$url"
		printf "output = %s\n" "$cachedir/${url##*/}"
done | curl -Z -K -
#sh_info "downloaded thumbnails..."

###########################
## user selection (nsxiv) ##
###########################
# extract the id's out of the thumbnail name
image_ids=$(nsxiv $nsxiv_args "$cachedir" | rev | cut -c5-10 | rev )
[ -z "$image_ids" ] && exit

#########################
## download wallpapers ##
#########################

# download the selected wall papers
# remove + and replace with underscore for folder name.
query=$(echo "$query" | sed 's/+/_/')
# if th folder does not exist - make it.
mkdir -p "$walldir$query";
# cd int the folder
cd "$walldir$query"
# show action dialog
sh_info "downloading wallpapers..."
# download the images to the folder
for ids in $image_ids
do
	ids="${ids##*/}"
	ids="${ids%.*}"
	url=$( jq -r '.data[]?|select( .id == "'$ids'" )|.path' < "$datafile" )
	printf "url = %s\n" "$url"
	printf -- "-O\n"
done | curl -K -

sh_info "wallpapers downloaded in:- '$walldir$query'"
