#!/bin/env bash

#Run game or given command in environment

cd "/media/Speedy/SteamLibrary/steamapps/common/Skyrim" || exit 0
DEF_CMD="/media/Speedy/Games/Skyrim_Mod_Tools/ModOrganizer/ModOrganizer.exe"
PATH="/media/Speedy/SteamLibrary/steamapps/common/Proton - Experimental/files/bin/:/usr/bin:/bin" \
	TERM="xterm" \
	WINEDEBUG="-all" \
	WINEDLLPATH="/media/Speedy/SteamLibrary/steamapps/common/Proton - Experimental/files/lib64//wine:/media/Speedy/SteamLibrary/steamapps/common/Proton - Experimental/files/lib//wine" \
	LD_LIBRARY_PATH="/home/supa/.local/share/Steam/ubuntu12_64/video/:/home/supa/.local/share/Steam/ubuntu12_32/video/:/media/Speedy/SteamLibrary/steamapps/common/Proton - Experimental/files/lib64/:/media/Speedy/SteamLibrary/steamapps/common/Proton - Experimental/files/lib/:/usr/lib/pressure-vessel/overrides/lib/x86_64-linux-gnu/aliases:/usr/lib/pressure-vessel/overrides/lib/i386-linux-gnu/aliases" \
	WINEPREFIX="/media/Speedy/SteamLibrary/steamapps/compatdata/72850/pfx/" \
	WINEESYNC="0" \
	WINEFSYNC="0" \
	SteamGameId="72850" \
	SteamAppId="72850" \
	WINEDLLOVERRIDES="steam.exe=b;dotnetfx35.exe=b;dotnetfx35setup.exe=b;beclient.dll=b,n;beclient_x64.dll=b,n;d3d11=n;d3d10core=n;d3d9=n;dxgi=n;d3d12=n" \
	STEAM_COMPAT_CLIENT_INSTALL_PATH="/home/supa/.local/share/Steam" \
	WINE_LARGE_ADDRESS_AWARE="1" \
	GST_PLUGIN_SYSTEM_PATH_1_0="/media/Speedy/SteamLibrary/steamapps/common/Proton - Experimental/files/lib64/gstreamer-1.0:/media/Speedy/SteamLibrary/steamapps/common/Proton - Experimental/files/lib/gstreamer-1.0" \
	WINE_GST_REGISTRY_DIR="/media/Speedy/SteamLibrary/steamapps/compatdata/72850/gstreamer-1.0/" \
	"/media/Speedy/SteamLibrary/steamapps/common/Proton - Experimental/files/bin/wine64" c:\\windows\\system32\\steam.exe "${@:-${DEF_CMD[@]}}"
