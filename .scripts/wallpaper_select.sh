#!/usr/bin/env bash
#
# Name: wallpaper_select.sh
# Description: Select a wallpaper from path defind with PumaMenu > "Wallpaper Set Theme Fodler".
# User can navigate to any folder using rofi filebrowser.
# Selected image file is sent to ~/.scripts/swallust.sh for processing.
# 
#set -x

# Where our wallpapers live.
wallpaperDir="$HOME/Pictures/"

# Check if path exists from PumaMenu, if not use default.
if [ -f "$HOME/.cache/wallpaper_theme_path" ]; then
  wallpaperDir="$(< ~/.cache/wallpaper_theme_path)"
elif [ -d "$HOME/Pictures/" ]; then
  wallpaperDir="$HOME/Pictures/"
fi

main() {
  rofi -no-config -theme fullscreen-preview.rasi -show filebrowser \
  -filebrowser-command "swallust.sh" \
  -filebrowser-directory "$wallpaperDir" \
  -filebrowser-sorting-method mtime -selected-row \
  >/dev/null
}

if pidof rofi > /dev/null; then
  pkill rofi
  exit 0
fi

main
