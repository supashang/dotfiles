#!/bin/env bash

figlet "Miniconda Installer"

if [ ! -d "$HOME/miniconda3" ]; then
echo ":: Would you like to install Miniconda?"
echo "https://docs.anaconda.com/miniconda/"
    choice=$(gum choose "Yes" "No")
    if [ "$choice" == "Yes" ]; then
        cd "$HOME" || exit
        echo "Creating $HOME/miniconda/ folder"
        mkdir -p "$HOME/miniconda3"
        echo "Downloading miniconda install script..."
        wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
        echo "Download complete."
        clear
        echo "Running miniconda install script."
        chmod +x "$HOME/miniconda3/miniconda.sh"
        sleep 1
        cd "$HOME/miniconda3/" || exit
        ./miniconda.sh -b -u -p "$HOME/miniconda3/"

        # Add miniconda to shell configs
        echo "Adding miniconda to shell configs..."
        if [ -f "/usr/bin/bash" ]; then
            # For Bash users
            "$HOME/miniconda3/bin/conda" init bash
        fi

        if [ -f "/usr/bin/zsh" ]; then
           # For zsh users
           "$HOME/miniconda3/bin/conda" init zsh
        fi

        cd "$HOME" || exit
        echo "done."
        sleep 2

        source "$HOME/miniconda3/etc/profile.d/conda.sh"

        # Disable conda base.
        clear
        echo "Do you want to disable the miniconda base environmant? (recommended)"
        condaBase=$(gum choose "Yes" "No")
        if [ "$condaBase" == "Yes" ]; then
            conda config --set auto_activate_base false
            echo "miniconda base environment deactivated."
        else
            echo "miniconda base set to default."
        fi

        # done
        echo ":: miniconda installed."
        sleep 3
    fi
fi
