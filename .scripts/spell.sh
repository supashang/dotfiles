#!/bin/env bash
#
# Spell check a word
# Supa Shang: https://gitlab.com/supashang
# Dependencies: didyoumean, rofi
set -euo pipefail

# Present a Rofi window for user to enter a word they wish to spell.
word=$(rofi -i -dmenu -width 500 -p 'Spellcheck a word')

# If user doesn't pick anything then exit here.
if [[ -z "${word}" ]]; then
    exit 1
else
    # Run the users entered word against didyoumean and present a list of correct spellings for the user to select.
    # # The correct word is then copied into the clipboard after being selected.
    dym -c -n 10 "$word" | rofi -i -dmenu -width 500 fzf -l 20 -p "Select correct spelling" | xclip -selection clipboard
fi
