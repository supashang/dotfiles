#!/bin/env bash
#  ____  ____        _____ ___  ____   ____ _____
# / ___||  _ \      |  ___/ _ \|  _ \ / ___| ____|
# \___ \| | | |_____| |_ | | | | |_) | |  _|  _|
#  ___) | |_| |_____|  _|| |_| |  _ <| |_| | |___
# |____/|____/      |_|   \___/|_| \_\\____|_____|
#
# Description: This script will attempt to download and install Stable Diffusion WebUI Forge, setup custom conda environment, favourite extensions,
#              symlinks to model and image output folders.
# Dependencies: gum, git, figlet, miniconda
# Creator: Supa Shang
#
# Miniconda URL: https://docs.anaconda.com/miniconda/

set -e
# The place we store out Git projects
git_projects="$HOME/Projects"
# SD-Forge Git repo
sd_forge_git="https://github.com/lllyasviel/stable-diffusion-webui-forge.git"
# Installation directory
sd_forge_path="$HOME/Projects/stable-diffusion-webui-forge"
# The name of the conda environment to use
sd_conda_env="sd-forge"
# Custom models path. If you already have SD models or have them located outside of the default folder.
sd_models_path="/media/AI/stablediffusiom/models"
# Outputs folder. The place where all your images will be saved to once generated.
sd_outputs="$HOME/Private/Images/sd-forge"

err() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*" >&2
}

figlet "SD-FORGE"
#echo "Stable Diffusion WebUI Forge?"
echo "Search Models: https://civitai.com/models"
echo "Run Stable Diffusion WebUI Forge?"
run_choice=$(gum choose "Yes" "No")
if [ "$run_choice" = "No" ]; then
    echo "exiting..."
    exit
fi

# Check if miniconda is installed.
if ! command -v "$HOME/miniconda3/bin/conda"
then
   echo ":: miniconda IS NOT installed. Would you like to install it? This is required for running SD-Forge in an isolated Python environment. (recommended)"
    choice=$(gum choose "Yes" "No")
    if [ "$choice" = "Yes" ]; then

        # Install miniconda.
        miniconda_installer="$HOME/.scripts/install-miniconda.sh"
        if [ -f "$miniconda_installer" ]; then
            ("$miniconda_installer")
            echo ":: Run the SD-FORGE script again once miniconda is installed."
            sleep 5
            exit
        else
            err "ERROR: Could not locate the script: $miniconda_installer"
            sleep 8
            exit 1
        fi

    elif [ "$choice" = "No" ]; then
        echo "Exiting..."
        sleep 1
        exit
    fi
fi

# Check if SD-FORGE is installed.
if [ ! -d "$sd_forge_path" ]; then
    echo "It looks like the Stable Diffusion WebUI-Forge folder doesn't exist! Would you like to download it?"
    install_forge=$(gum choose "Yes" "No")
    if [ "$install_forge" == "No" ]; then
        echo "Goodbye friend!"
        sleep 2
        exit
    else
        # Check if git is installed.
        if ! command -v git
        then
            echo "Please install git in order to use this script!"
            exit 1
        fi

        # If Git projects folder does not exist - create it.
        if [ ! -d "$git_projects" ]; then
            echo "Creating $git_projects folder..."
            mkdir "$git_projects"
        fi

        sleep 1

        # Change current directory to install sd-forge.
        echo "Changing working directory to $HOME/Projects/ for installation..."
        cd "$HOME/Projects/" || exit

        # Clone the git repo
        echo "Cloning $sd_forge_git to $git_projects"
        git clone $sd_forge_git
        if [ $? -gt 0 ]; then
            echo ":: Could not download!"; exit 1
        else
            echo "Downloading extensions..."
            cd "$sd_forge_path/extensions/" || exit

            sleep 1

            # Download SD extensions to extensions folder.
            git clone https://github.com/Bing-su/adetailer.git
            #git clone https://github.com/Gourieff/sd-webui-reactor.git
            git clone https://github.com/thomasasfk/sd-webui-aspect-ratio-helper.git
            echo ":: Extensions downloaded."
        fi

        cd "$sd_forge_path" || exit

        # Create Symlinks
        if [ "$sd_models_path" != "" ]; then
            echo "Create symlinks to custom models and outputs folders?"
            model_choice=$(gum choose "Yes" "No")
            if [ "$model_choice" == "Yes" ]; then
                cd "$sd_forge_path" || exit
                sleep 1
                # Set custom model path.
                if [ -d "$sd_models_path" ]; then
                    echo "Removing default models folder"
                    rm -rf models
                    echo "Creating symlink to custom models folder..."
                    ln -s "$sd_models_path" models
                fi
                # Set cutom output path.
                if [ "$sd_outputs" != "" ]; then
                    echo "Removing default outputs folder"
                    rm -rf "$sd_forge_path/outputs"
                    echo "Creating symlink to custom image outputs folder..."
                    ln -s "$sd_outputs" outputs
                fi
            fi
        fi
    fi
fi

cd "$sd_forge_path" || exit
source $HOME/miniconda3/etc/profile.d/conda.sh
sleep 3
if [ -d "$HOME/miniconda3/envs/sd-forge" ]; then
    conda activate $sd_conda_env
else
    # Create the conda environment and install Python 3.10
    echo "Installing isolated Python 3.10 environment for Stable Diffusion WebUI Forge..."
    conda create --name $sd_conda_env python=3.10
    # Activate the conda environment
    sleep 2
    conda activate $sd_conda_env
    # Install missing dependencies
    echo "Installing gcc..."
    conda install -c conda-forge gcc=12.1.0
fi

#Launch sd-forge
echo "Launching..."
./webui.sh --cuda-malloc
