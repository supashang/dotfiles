#!/bin/env bash

# Creates proportionally scaled images to thumbnails using ffmpeg.
# I use this script to create thumbnail images for my website.
set -euo pipefail

# Set the destination folder of the thumbnail images prefixed by the current year.
dest="./$(date -u +"%Y")-thumbs"

# If the destination folder doesn't exist - create it.
if [[ ! -d "$dest" ]]; then
    notify-send "creating \"$dest\" folder"
    mkdir "$dest"
fi

# Pass files through ffmpeg and scale to new size, then save to dest output folder.
for file
    do
    if [ ! -e "$file" ]
        then
        continue
    fi
    to_name="$dest/${file%.*}-thumb.png"
    ffmpeg -i "${file}" -vf scale=310:ih*240/iw "${to_name}"
done && notify-send "Thumbnail conversion completed"
