#!/bin/env bash

#set -e

# Check Hyprland animations.
hypr_gamemode=$(hyprctl getoption animations:enabled | awk 'NR==1{print $2}')
# Is variety running?
variety_pid=$(pgrep variety)

if [ "$XDG_SESSION_DESKTOP" = "Hyprland" ] || [ "$DESKTOP_SESSION" == "hyprland" ]; then
    if [ "$hypr_gamemode" = 1 ]; then

    # Gamemode: On
    if [ "$variety_pid" ]; then
        variety --pause &
    fi

    hyprctl --batch "\
        keyword animations:enabled 0;\
        keyword decoration:drop_shadow 0;\
        keyword decoration:blur:enabled 0;\
        keyword general:gaps_in 0;\
        keyword general:gaps_out 0;\
        keyword general:border_size 1;\
        keyword decoration:rounding 0"

        notify-send -i "/usr/share/icons/gnome/32x32/apps/octopi.png" "GAME MODE: ENABLED"

    exit
    else
    # Gamemode: Off

      # Check if variety is running. If it is, then enable random wallpaper.
        if [ "$variety_pid" ]; then
            variety --resume &
        fi

        notify-send -i "/usr/share/icons/Adwaita/symbolic/categories/applications-games-symbolic.svg" "GAME MODE: DISABLED"
    fi
fi

hyprctl reload
