#!/bin/env bash
#                _ _
# __      ____ _| | |_ __   __ _ _ __   ___ _ __
# \ \ /\ / / _` | | | '_ \ / _` | '_ \ / _ \ '__|
#  \ V  V / (_| | | | |_) | (_| | |_) |  __/ |
#   \_/\_/ \__,_|_|_| .__/ \__,_| .__/ \___|_|
#                   |_|         |_|
#                      _
#  _ __ __ _ _ __   __| | ___  _ __ ___
# | '__/ _` | '_ \ / _` |/ _ \| '_ ` _ \
# | | | (_| | | | | (_| | (_) | | | | | |
# |_|  \__,_|_| |_|\__,_|\___/|_| |_| |_|
#
# by: Supa Shang
# Description: Set a random wallpaper from a path in: ~/.cache/wallpaper_theme_path
#              with a choice of wallpaper tools.
#
#              This is used after first running: $HOME/.scripts/wallpaper_folder_path.sh
#              Use Super +z (Pumamenu) - (Wallpaper - 'Set Theme Folder') to select a image path.
#
#              Bind this script to your hotkeys.
#              eg, Hyprland:
#
#              bind = $mainMod, P, exec, wallpaper_random.sh # Next random wallpaper
#
# Dependencies: fd, wallust, pywal, shuf, feh, swww, variety, wpaperd

set -x

# Use wallust or pywal to set your terminal colours to match the wallpaper colour. (true or false)
term_colors=true

# Get the current wallpaper path set by PumaMenu (Super + z) Wallpaper - Set Theme Folder
wptp="$HOME/.cache/wallpaper_theme_path"
if [ -f "$wptp" ]; then
    wallpaper_theme_path=$(< "$wptp")
elif [ -d "$HOME/Pictures/" ]; then
    # If that file doesn't exist - just use Pictures path:
    wallpaper_theme_path="$HOME/Pictures/"
else
    # If all above don't exist, use default path:
    wallpaper_theme_path="/usr/share/hypr/"
fi

# Error Handler: If a folder is on an unmounted drive:
if [ ! -d "$wallpaper_theme_path" ]; then
    notify-send -i "/usr/share/icons/gnome/24x24/apps/preferences-desktop-wallpaper.png" "Folder Not Found:" "$wallpaper_theme_path"
    exit
fi

# Get a random wallpaper from path using fd, plus don't spam multiple instances of fd
# Don't get a wallpaper if 'wpaperd' is running (It looks for its own from its config 'path =' location.)
if [ -n "$(pidof fd)" ]; then
    exit 1
else
    # If not running wpaperd, get a random image.
    if [ -z "$(pidof wpaperd)" ]; then
        # Set wallpaper path as a variable.
        wallpaper=$(fd . -t f -L "$wallpaper_theme_path" | shuf -n 1)
        
        # Copy the full wallpaper image path to a text file, for future use with scripts.
        echo "$wallpaper" > "/tmp/current_wallpaper"

        # change terminal colors to wallpaper theme
        if [ "$term_colors" == true ]; then
        #wal -i "$wallpaper"
        wallust -q run "$wallpaper"
        fi
    fi
fi

# Check desktop session.
if [ "$DESKTOP_SESSION" == "awesome" ]; then
    # Set wallpaper using feh on Xorg
    feh --no-fehbg --bg-max "$wallpaper"
elif [ "$DESKTOP_SESSION" == "hyprland" ] || [ "$XDG_SESSION_DESKTOP" == "Hyprland" ] || [ "$DESKTOP_SESSION" == "river" ]; then

    # If wallpaper tools are running use them.
    if [ -n "$(pgrep variety)" ]; then
        # You have two options here. Only enable one.
        # 1. Have variety set a wallpaper chosen from this scripts path:
        variety --set "$wallpaper"
        # 2. Have variety use the next walpaper as defined in its own settings:
        #variety --next
    elif [ -n "$(pgrep wpaperd)" ]; then
        # Set new wallpapers using wpaperd.
        wpaperctl next
    elif [ -n "$(pidof swww-daemon)" ]; then
        # Check CPU usage.
        cpuUsage=$(ps -A -o %cpu | awk '{s+=$1} END {print s}')
        cpuLimit=40.0
        # Option1 - High CPU usage.
        # Don't use complex transition if CPU usage is high.
        if (( $(echo "$cpuUsage > $cpuLimit" | bc -l) )); then
            swww img --resize fit --transition-type=none "$wallpaper";
        else
            # Option2 - Low CPU usage.
            swww img --resize fit --transition-type=random "$wallpaper";
        fi
    fi
fi
