#!/bin/env bash
#
# YouTube Subs Search
# Supa Shang: https://gitlab.com/supashang
# Reads the file yt-subs.txt in your Documents folder and allows you to select a YT channel via Rofi, which then opens in the Brave browser.
# I call this script with "Super + Shift + u"
# Dependencies: awk, rofi, brave-bin

# Text file containing YouTube channel names and urls. Separate the name and url with a TAB.
# For example: DistroTube [Tab] https://www.youtube.com/c/DistroTube/videos
ytchans="$HOME/Documents/yt-subs.txt"
# Print the channel names and urls in rofi
name=$(awk '{print $1, " : " $NF}' FS='\t' "$ytchans" | rofi -i -dmenu -width 1500 -sorting-method fzf -l 20 -p 'YouTube Channels')
# If user doesn't pick anything then exit here.
if [ -z "${name}" ]; then
    exit 1
else
    # Filter out the url and set as a variable to use with brave.
    url="$(echo "$name" | awk '{print $NF}')"
    # Notify user
    notify-send "Opening: $name"
    # Load browser with selected URL
    brave "$url"
fi
