#!/bin/env bash
#
# Kill all WINE processes.
#
# You can use this method.
# wineserver -k

for pid in $(pgrep '(wine|processid|\.exe)'); do
    kill -9 "$pid"
done
