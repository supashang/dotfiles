#!/bin/env bash
# PyWal theme setter, using feh to set wallpaper.
# by Supa Shang
# Description: Uses wal to set the terminal colours from a random wallpaper pulled from path in .feh_walpath
# This is a daughter-script to .scripts/feh_rofi_wallpaper.sh
# #Dependencies: python-pywal, feh

set -euo pipefail

wal -n -i "$(< "${HOME}/.feh_walpath")"
feh --bg-max "$(< "${HOME}/.cache/wal/wal")"
