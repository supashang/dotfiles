#!/bin/env bash

remote_id=$(
  xinput list |
  sed -n 's/.*Naga.*id=\([0-9]*\).*keyboard.*/\1/p'
)
[ "$remote_id" ] || exit

mkdir -p /tmp/xkb/symbols
cat >/tmp/xkb/symbols/custom <<\EOF
xkb_symbols "remote" {
    key <AE01>   {      [0xff55]     };
    key <AE02>   {      [0x1008FF57] };
    key <AE03>   {      [0x33]       };
    key <AE04>   {      [0xff56]     };
    key <AE05>   {      [0xff51]     };
    key <AE06>   {      [0xff53]     };
    key <AE07>   {      [0x1008FF16] };
    key <AE08>   {      [0x1008FF31] };
    key <AE09>   {      [0x1008FF17] };
    key <AE10>   {      [0x30]       };
    key <AE11>   {      [0x2d]       };
    key <AE12>   {      [0x3d]       };
};
EOF

setxkbmap -device $remote_id -print | sed 's/\(xkb_symbols.*\)"/\1+custom(remote)"/' | xkbcomp -I/tmp/xkb -i $remote_id -synch - $DISPLAY 2>/dev/null
notify-send -i "/usr/share/icons/gnome/22x22/devices/gnome-dev-mouse-ball.png" "Razer Naga Config" "Set to Firefox button mapping."
