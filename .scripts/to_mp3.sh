#!/bin/env bash
#
# Converts video files to .mp3
set -euo pipefail

dest="./mp3_converted"

if [[ ! -d "$dest" ]]; then
    notify-send "creating \"$dest\" folder"
    mkdir "$dest"
fi

for file
    do
    if [ ! -e "$file" ]
        then
        continue
    fi
    to_name="$dest/${file%.*}.mp3"
    ffmpeg -i "${file}" -f mp3 "${to_name}"
done && notify-send "Audio conversion completed"
