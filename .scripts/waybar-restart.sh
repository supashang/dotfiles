#!/bin/env bash
# Restart waybar if loaded or run it if it isn't.

waybar_pid=$(pgrep waybar)

if [ "$XDG_SESSION_DESKTOP" == "Hyprland" ]; then
    if [ -n "$waybar_pid" ]; then
        notify-send "Waybar: Restart..."
        killall -SIGUSR2 waybar
    else
        uwsm app -- waybar -c "$HOME/.config/waybar/hyprland-config" -s "$HOME/.config/waybar/hyprland-style.css"
    fi
elif [ "$DESKTOP_SESSION" == "hyprland" ]; then
    if [ -n "$waybar_pid" ]; then
        notify-send "Waybar: Restart..."
        killall -SIGUSR2 waybar
    else
        waybar -c "$HOME/.config/waybar/hyprland-config" -s "$HOME/.config/waybar/hyprland-style.css"
    fi
fi
