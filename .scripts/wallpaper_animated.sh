#!/bin/env bash
#
# Animated wallpaper script.
# Credits: https://www.youtube.com/channel/UCngn7SVujlvskHRvRKc1cTw
# Dependencies: rofi, mpv, xwinwrap-baitinq-git, exa
#
#set -euo pipefail

# !Hack! Rofi don't like being launched inside rofi, so kill the last rofi before running this scipt which loads rofi!
kill "$(pgrep rofi)"

apps_to_check=("mpvpaper" "wpaperd" "swww-daemon" "swaybg" "hyprpaper" "variety")

# The resolution you want the animation to play at.
mainScreenRez=2560x1440
# If you have more than one monitor, set the maximun horizontal resolution of your left monitor below to have the animation play on your middle screen, else set below to 0+0 for single monitor
offset=1680+0

# Path to animated wallpaper video files
wallpath="$HOME/Videos/Live_Wallpapers/"
# Find videos folder
if [[ ! -d "$wallpath" ]]; then
    # If the videos folder doesn't exist - inform the user.
    notify-send "Animated Wallpaper Error:" "Folder \"$wallpath\" does not exists."
    exit 1
else
    # cd to the video path
    cd "$wallpath" || exit 1
    # Use rofi to present a list of animated wallpaper files. The selected file will be used as the animated wallpaper.
    #selected=$(exa "$wallpath" | rofi -dmenu -i -p "Animated Wallpaper: ")
    
    selected=$(fd . -t f -L "$wallpath" | rofi -dmenu -i -p "Animated Wallpaper: ")
    if [[ -z "$selected" ]]; then
        notify-send "Nothing selected"
        exit 1
    else
        notify-send "Selected: \"$selected\""
        if [ "$DESKTOP_SESSION" == "awesome" ]; then
            # Is xwinwrap already running? Kill it.
            kill "$(pgrep xwinwrap)"
            # # You may need to edit the resolution below for your display configuration.
            xwinwrap -g "$mainScreenRez"+"$offset" -ov -ni -- mpv --fullscreen --no-stop-screensaver --loop --no-audio --no-border --no-osc --no-osd-bar --no-input-default-bindings -wid WID "$selected" > /dev/null 2>&1 &
        elif [ "$XDG_SESSION_DESKTOP" == "Hyprland" ] || [ "$DESKTOP_SESSION" == "hyprland" ] || [ "$DESKTOP_SESSION" == "river" ]; then

            # Kill the apps if they're running
            for app in "${apps_to_check[@]}"; do
                if pgrep -f "$app" > /dev/null; then
                    #echo "Killing $app..."
                    killall -9 "$app"
                fi
            done

            uwsm app -- mpvpaper -vs -o "no-audio loop" DP-1 "$selected"
        fi
    fi
fi
