#!/bin/env bash
#
# Config Editor
# Supa Shang: https://gitlab.com/supashang
# Reads the ~/.scripts/dots_data.txt file and allows you to edit the selected file.
# Dependencies: awk, rofi, $VISUAL variable set in ~/.profile

set -euo pipefail

# Text file containing config names and paths. Separate the name, path and url with ;;
cfgpaths="$HOME/.scripts/dots_data.txt"
# Print the config names and paths in rofi
cfgname=$(awk '{print $1}' FS=';;' "$cfgpaths" | rofi -i -dmenu -width 1500 -sorting-method fzf -l 20 -p '✍ Edit Config')

# If user doesn't pick anything then exit here.
if [[ -z "${cfgname}" ]]; then
    exit 1
else
    # Filter out just the path and set as a variable to use with the editor.
    localconf="$(awk -F";;" -v lpat="^$cfgname$" '$1 ~ lpat {print $2}' "$cfgpaths")"

    # Check if the local file exists. If not, then exit here.
    if [ ! -f "$HOME/$localconf" ]; then
        notify-send "!! ERROR !!" "$localconf DOES NOT exist!"
        exit 2
    else
        notify-send "Opening" "$localconf"
        # Open the selected file with $VISUAL editor set in .profile
        $VISUAL "$HOME/$localconf"
    fi
fi
