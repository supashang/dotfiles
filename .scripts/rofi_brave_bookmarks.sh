#!/bin/env bash
#
# Brave Bookmarks Search
# Supa Shang: https://gitlab.com/supashang
# Reads the default Brave "Bookmarks" JSON file and feeds url results into Rofi.
# Dependencies: jq, rofi, brave-browser
#
set -euo pipefail

# Default Brave bookmarks file location.
bmks="$HOME/.config/BraveSoftware/Brave-Browser/Default/Bookmarks"
# Use jq to filter the urls and pipe into rofi
url=$(jq -cr '.roots[]?.children[]?.children[]? | {url} | join(" 🌐 ")' "$bmks" | sort -u | rofi -i -dmenu -width 1500 -sorting-method fzf -l 20 -p 'Brave Bookmarks')
# If user doesn't pick anything then exit here.
if [[ -z "${url// }" ]]; then
    exit 1
fi

# If the default browser is brave - open the URL.
if [ "$XDG_SESSION_DESKTOP" == "Hyprland" ]; then
    uwsm app -- brave "$url"
else
    brave "url"
fi
