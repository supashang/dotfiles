#!/bin/env bash
#
# Local Music Video script.
# Dependencies: rofi, mpv, exa
#
#set -euo pipefail

# !Hack! Rofi don't like being launched inside rofi, so kill the last rofi before running this scipt which loads rofi! phew!
kill "$(pgrep rofi)"

# Path to music video files
vidpath="/home/supa/Videos/Music_Videos"
# Find videos folder
if [[ ! -d "$vidpath" ]]; then
    # If the videos folder doesn't exist - inform the user.
    notify-send "Video Player Error:" "Folder \"$vidpath\" not found."
    exit 1
else
    # cd to the video path
    cd "$vidpath" || exit 1
    # Use rofi to present a list of video files. The selected file will be played.
    selected=$(eza -f -1 --icons=never --no-quotes "$vidpath" | rofi -dmenu -i -p "Videos: ")
    if [[ -z "$selected" ]]; then
        notify-send "Nothing selected"
        exit 1
    else
        notify-send "Selected: \"$selected\""
        # Is already running process.
        kill "$(pgrep mpv)"
        # Play video using mpv.
        mpv --fullscreen "$selected" > /dev/null 2>&1 &
    fi
fi
