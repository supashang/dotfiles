#!/bin/env bash

# - To add programs to autorun, append run program [some arguments] as shown below.
# - The run function checks whether there already is an instance of program running and only runs program if there is none.
# - You can check your autorun.sh by running it in a terminal.
# - Add a # at the start of a line to disable a command from being executed.
# - You don't need to add & at the end of every run command as there is one in the run function.
#
function run {
if ! [[ "$(pgrep -f "$1")" ]]; then
  "$@" &
fi
}

# Polkit from LXSession
run lxsession
# Set screen position on multihead with arandr
run nm-applet
# Volume Control applet
run awesome_multihead_screenlayout.sh
# Compositor (replaces any picom forks)
#run compfy
# picom
#run picom
# sxhkd Hotkeys
run sxhkd
# Network Manager Applet
run volctl
# Playerctld daemon
run playerctld daemon
# dunst
run dunst > /dev/null 2>&1
# Set Numlock key to active.
run numlockx
# clipit
#run clipit
# copyq clipboard manager
#run copyq
# Unclutter - (hides mouse pointer after 5 seconds of inactivity)
run unclutter
# Emacs daemon
run emacs --daemon
# Thunar daemon
run thunar --daemon
# MPD
run mpd "$HOME/.config/mpd/mpd.conf"
# udiskie drive mounter
run udiskie -tN --no-password-cache --no-automount --smart-tray
# Set wallpaper
run "$HOME/.fehbg"
# Variety Wallpaper Changer
#run variety
# Flameshot screenshot
run flameshot
# Dropbox
run dropbox
# Telegram
run telegram-desktop -startintray
# XScreensaver
#run xscreensaver --no-splash
# Screensaver off
#run xset s off
# Mailspring
#run mailspring --background %U
# Bluetooth
#run blueman-tray
# GreenWithEnvy NVIDIA App
#run gwe --hide-window
