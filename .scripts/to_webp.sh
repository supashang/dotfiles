#!/bin/env bash
#
# Converts selected images in Thunar to webp.
# 
# ** How TO **
#
# Add this file into a Thunar custom action.
# In the Command string enter,
# ~/.scripts/thunar/to_WEBP_images.sh %N
# In Appearance Conditions, tick "Images" 
# In File Pattern type *
set -euo pipefail

# Name of folder to store converted images.
dest="./webp_converted"

# If the destination folder doesn't exist - create it.
if [[ ! -d "$dest" ]]; then
    notify-send "creating \"$dest\" folder"
    mkdir "$dest"
fi

# Pass files through ffmpeg and scale to new size, then save to dest output folder.
for file
    do
    if [ ! -e "$file" ]
        then
        continue
    fi
    to_name="$dest/${file%.*}.webp"
    cwebp -q 60 "${file}" -o "${to_name}"
done && notify-send "Image conversion completed"
