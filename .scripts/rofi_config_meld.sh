#!/bin/env bash
#                   __ _                          _     _
#   ___ ___  _ __  / _(_) __ _     _ __ ___   ___| | __| |
#  / __/ _ \| '_ \| |_| |/ _` |   | '_ ` _ \ / _ \ |/ _` |
# | (_| (_) | | | |  _| | (_| |   | | | | | |  __/ | (_| |
#  \___\___/|_| |_|_| |_|\__, |___|_| |_| |_|\___|_|\__,_|
#                        |___/_____|
#
# Supa Shang: https://gitlab.com/supashang
# Compare local config files or scripts with the latest ones from my GitLab.
# This script was written for me to quickly pull down my config files from GitLab and overwrite configs in my VM's $HOME.
# Dependencies: awk, rofi, curl, meld, zenity
#
# !!! THIS IS A VERY DANGEROUS SCRIPT !!!
# !!! DO NOT RUN THIS SCRIPT ON A LIVE SYSTEM !!!
# !!! UNLESS YOU KNOW WHAT YOU ARE DOING !!!
# !!! YOU HAVE BEEN WARNED !!!
#
# Fail on error: https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -euo pipefail
# Default text file containing config label, local path and URL to remote file. Separate fields with ";;".
config_paths="$HOME/.scripts/dots_data.txt"
# Print the config labels into rofi from the file.
name=$(awk -F";;" '{print $1}' "$config_paths" | rofi -i -dmenu -show -l 20 -p '💻 Update Config')
# If user doesn't pick anything then exit here.
if [ -z "${name}" ]; then
    exit
else
    # Use awk to find our selected config label in the dots_data.txt file and extract its local path and URL.
    localconf=$(awk -F";;" -v lpat="^$name$" '$1 ~ lpat {print $2}' "$config_paths")
    remoteconf=$(awk -F";;" -v rpat="^$name$" '$1 ~ rpat {print $3}' "$config_paths")

    # Check if the local file exists. If not, then prompt user.
    if [ ! -f "$HOME/$localconf" ]; then
        notify-send "ERROR!" "$localconf DOES NOT exist!"
        if zenity --question \
            --text="File DOES NOT exist! Create it?" \
            --title="$localconf";
        then
            cfgpath=$(dirname "$HOME/$localconf")
            # Yes - create folder.
            if [ ! -d  "$cfgpath" ]; then
                mkdir -p "$cfgpath" || { echo "Failed to create $cfgpath"; exit 1; }
            fi
            # Create the file if the parent paths exist.
            if [ -d "$cfgpath" ]; then
                touch "$localconf" || { echo "Failed to create $localconf"; exit 1; }
            fi
        else
            # No!
            exit 2
        fi
    fi

    # Check if there is a URL associated with this entry. If not, notify user and exit.
    if [ -z "$remoteconf" ];
    then
        notify-send "There is no external link to this file."
        exit 2
    fi

    # If our working directory doesn't exist - create it.
    cfgtmp="$HOME/.cache/supa_configs/$name"
    if [ ! -d "$cfgtmp" ]; then
        notify-send "Creating cache config folder: $cfgtmp"
        mkdir -p "$cfgtmp"
    fi

    # Change directory to where we want to work with the config file.
    cd "$cfgtmp" || exit 2
    # Download the remote file to .cache folder.
    curl -s -O "$remoteconf";
    # Open meld to compare the two files.
    meld "${localconf##*/}" "$HOME/$localconf"

    sleep 1

    # Check if that file was a Bash script and prompt user to set its executable flag if needed.
    file_type=$(file -b "$HOME/$localconf")
    if [ "$file_type" == 'a /bin/env bash script, ASCII text executable' ] && [ ! -x "$HOME/$localconf" ]; then
        if zenity --question \
             --text="Make this script executable?" \
             --title="$HOME/$localconf"; then

             chmod +x "$HOME/$localconf"
        fi
    fi
fi
