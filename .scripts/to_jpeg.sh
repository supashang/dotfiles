#!/bin/env bash
#
# Converts selected images to jpeg height 1050
# pixels size using ffmpeg.
# 
# ** How TO **
#
# Add this file into a Thunar custom action.
# In the Command string enter,
# ~/.scripts/to_jpeg.sh %N
# In Appearance Conditions, tick "Images" 
# In File Pattern type *

set -euo pipefail

# Set image output vertical resolution.
vrez=512

# Path to save converted images
dest="./jpg_converted"

if [[ ! -d "$dest" ]]; then
    notify-send "creating \"$dest\" folder"
    mkdir "$dest"
fi

# Pass files through ffmpeg and scale to new size, then save to dest output folder.
for file
    do
    if [ ! -e "$file" ]
        then
        continue
    fi
    to_name="$dest/${file%.*}.jpg"
    ffmpeg -i "${file}" -vf scale=-1:$vrez "${to_name}"
done && notify-send "Image conversion completed"
