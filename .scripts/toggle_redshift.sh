#!/bin/env bash
#
# Toggle redshift on/off to a given setting.
#
# File path to store variable
rstog="$HOME/.cache/redshifttoggle"

# If the redshift variable file doesn't exist - create it.
if [ "$DESKTOP_SESSION" == "awesome" ]; then
    if [ -f "$rstog" ]; then
        rm "$rstog"
        redshift -x
    else
        redshift -P -O 2600
        touch "$rstog"
    fi
fi
