#!/bin/env bash
#
# Converts images to Lutris banner size using ffmpeg. (184:69)

set -euo pipefail

dest="./Lutris_converted"

if [[ ! -d "$dest" ]]; then
    notify-send "creating \"$dest\" folder"
    mkdir "$dest"
fi

# Pass files through ffmpeg and scale to new size, then save to dest output folder.
for file
    do
    if [ ! -e "$file" ]
        then
        continue
    fi
    to_name="$dest/"${file%.*}".jpg"
    ffmpeg -i "${file}" -vf scale=184:69 "${to_name}"
done && notify-send "Image conversion completed"
