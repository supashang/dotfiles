#!/bin/env bash
#
# Rofi Web Search
# Search web via Rofi
# Dependencies: ddgr, jq, rofi

set -euo pipefail

action=$(rofi -dmenu -p "Web Search: ")

search () {
  query=$1
  index=$2

  sel=$(printf "%s" "$(ddgr -n 15 --json "$1" | jq '.[].url' | sed -e 's/^"//' -e 's/"$//' -e 's/ /+/')\nMore\n" | rofi -dmenu)

  case $sel in
    "") exit ;;
    "More") search "$query" "$( ( $index + 15 ) )" ;;
    *) xdg-open "$sel" ;;
  esac
}

search "$action" 0
