#!/bin/env bash
#
# Run instances of Radegast SL client
#
rrg="$HOME/.Radegast/Radegast.exe"

if [ ! -f "$rrg" ]; then
    notify-send "Radegast folder wasn't found."
    exit
else
    notify-send "loading Radegast: $i $rrg"
    for i in {1..4}; do
    wine "$rrg" &
    done
fi
