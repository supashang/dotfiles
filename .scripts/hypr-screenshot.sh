#!/bin/env bash
#  _                                                            _           _
# | |__  _   _ _ __  _ __      ___  ___ _ __ ___  ___ _ __  ___| |__   ___ | |_
# | '_ \| | | | '_ \| '__|____/ __|/ __| '__/ _ \/ _ \ '_ \/ __| '_ \ / _ \| __|
# | | | | |_| | |_) | | |_____\__ \ (__| | |  __/  __/ | | \__ \ | | | (_) | |_
# |_| |_|\__, | .__/|_|       |___/\___|_|  \___|\___|_| |_|___/_| |_|\___/ \__|
#        |___/|_|
#
# by Supa Shang
# Description: Prompts user with options to take image or video snapshots of a screen.
# Dependecies: hyprshot, slurp, rofi, wf-recorder, swappy, xclip, thunar,

# Path to save screenshots.
image_dir="$HOME/Pictures/screenshots"
# Share screenshots to a friend in shared Dropbox folder.
share_dir="$HOME/Dropbox/Sleezy_Shared/screenshots"
# Create filename with date.
image_name="screenshot_$(date +%d%m%Y_%H%M%S).png"
# Screen to capture video from.
vid_screen="DP-1"
# Set a delay before capture for Area Only
delay=5

# Kill video screen recorder if recording.
wf_rec="$(pgrep wf-recorder)"
if [ -n "$wf_rec" ]; then
    kill "$wf_rec"
    notify-send "Stopped Video Recording."
    exit
fi

# Rofi menu options.
option2="Image - Area"
option3="Image - Area (5 second delay)"
option4="Image - Fullscreen"
option5="Image - Window"
option6="Image - Share to Dropbox"
option7="Image - Scribble"
option8="Image - To Clipboard"
option9="Video - Record Fullscreen (no sound)"
option10="Video - Record Fullscreen (with sound)"
option11="Video - Record Region (no sound)"
option12="Video - Record Region (with sound)"
option13="Video - File Browse"

# Group the menu options.
options="$option2\n$option3\n$option4\n$option5\n$option6\n$option7\n$option8\n$option9\n$option10\n$option11\n$option12\n$option13"
# Show Rofi and present options.
choice=$(echo -e "$options" | rofi -dmenu -i -no-show-icons -l 9 -width 30 -p "Take Screenshot")
# Do action on option selection.
case $choice in
    "$option2")
        # Area
        hyprshot -z -m region -o "$image_dir"
    ;;
    "$option3")
        # Area
        if [ "$delay" -gt 0 ];then
            notify-send "Area capture in 5 seconds..."
            sleep "$delay"
        fi
        hyprshot -z -m region -o "$image_dir"
    ;;
    "$option4")
        # Fullscreen
        hyprshot -z -m output -o "$image_dir"
    ;;
    "$option5")
        # Window
        hyprshot -z -m window -o "$image_dir"
    ;;
    "$option6")
        # To Dropbox
        hyprshot -z -m output -o "$share_dir"
    ;;
    "$option7")
        # Scribble
        grim -g "$(slurp)" "$image_dir/$image_name"
        xclip -selection clipboard -t image/png -i "$image_dir/$image_name"
        notify-send "Screenshot saved and copied to clipboard" "Mode: Scribble"
        swappy -f "$image_dir/$image_name"
    ;;
    "$option8")
        # Clipboard
        hyprshot -z -m output --clipboard-only
    ;;
    "$option9")
        # Video Fullscreen (no sound)
        wf-recorder -o "$vid_screen" -f "$HOME/Videos/$(whoami)_screenclip_$(date +%Y_%m_%d_%H-%M-%S).mp4"
    ;;
    "$option10")
        # Video Fullscreen (with sound)
        wf-recorder -a -o "$vid_screen" -f "$HOME/Videos/$(whoami)_screenclip_$(date +%Y_%m_%d_%H-%M-%S).mp4"
    ;;
    "$option11")
        # Video Region (no sound)
        wf-recorder -g "$(slurp)" -o "$vid_screen" -f "$HOME/Videos/$(whoami)_screenclip_$(date +%Y_%m_%d_%H-%M-%S).mp4"
    ;;
    "$option12")
        # Video Region (with sound)
        wf-recorder -a -g "$(slurp)" -o "$vid_screen" -f "$HOME/Videos/$(whoami)_screenclip_$(date +%Y_%m_%d_%H-%M-%S).mp4"
    ;;
    "$option13")
        # Video Browse
        nohup thunar "$HOME/Videos/"
    ;;
esac
