#!/bin/env bash
#  ___           _        _ _   _   _           _       _
# |_ _|_ __  ___| |_ __ _| | | | | | |_ __   __| | __ _| |_ ___  ___
#  | || '_ \/ __| __/ _` | | | | | | | '_ \ / _` |/ _` | __/ _ \/ __|
#  | || | | \__ \ || (_| | | | | |_| | |_) | (_| | (_| | ||  __/\__ \
# |___|_| |_|___/\__\__,_|_|_|  \___/| .__/ \__,_|\__,_|\__\___||___/
#                                    |_|
#
# Dependencies: gum, figlet, yay, topgrade, timeshift
# Description: This script can be bound to a hotkey.
#              It will provide the user with an option to perform a timeshift backup prior to updating with yay or topgrade.

figlet "Updates"

isInstalled() {
    package="$1";
    check="$(yay -Qs --color always "${package}" | grep "local" | grep "${package} ")";
    if [ -n "${check}" ] ; then
        echo 0; #'0' means 'true' in Bash
        return; #true
    fi;
    echo 1; #'1' means 'false' in Bash
    return; #false
}


# Confirm
if gum confirm "Hello $USER, would you like to update your system packages?" ;then
    echo ":: Update started."
elif [ $? -eq 130 ]; then
        exit 130
else
    echo ":: Update canceled."
    if [[ -f "$SFX_EXIT" && $SOUNDS == "true" ]]; then
        aplay -q "$SFX_EXIT"
    fi
    exit;
fi

# Snapshot
if [[ $(isInstalled "timeshift") == "0" ]] ;then
    if gum confirm "Do you want to create a timeshift snapshot?" ;then
        echo
        c=$(gum input --placeholder "Enter a comment for the snapshot...")
        sudo timeshift --create --comments "$c"
        sudo timeshift --list
        sudo grub-mkconfig -o /boot/grub/grub.cfg
        echo ":: DONE. Snapshot $c created!"
        echo
    elif [ $? -eq 130 ]; then
        echo ":: Snapshot canceled."
        exit 130
    else
        echo ":: Snapshot canceled."
    fi
    echo
fi

# Upgrade with yay or topgrade.
if [ -f "/usr/bin/topgrade" ]; then
    echo ":: Choose what you would like to upgrade..."
    choice=$(gum choose "System Packages" "Everything (topgrade)")
    if [ "$choice" == "System Packages" ];then
        # Password promt
        figlet "yay"
        echo ":: Please enter password..."
        yay
    elif [ "$choice" == "Everything (topgrade)" ];then
        # Password promt
        figlet "topgrade"
        echo ":: Please enter password..."
        topgrade
    fi
else
    echo ":: Please enter password..."
    yay
fi

# Done
notify-send "Update complete"

# Show info on packages
yay -Ps

echo ":: Update complete"

# Restart Waybar to clear old update count.
source ~/.scripts/waybar-restart.sh &

# Pause so user can review update log
if gum choose "Exit" ;then
    exit
fi
