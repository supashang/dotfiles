#!/bin/env bash

# Calculator toggle
# Description: Runs it if it ain't - quits it if it is.

function run {
    if ! pgrep -f "$1" ;
    then
        "$@"&
    else
        pgrep -f "$1" | xargs kill
    fi
}

run qalculate-gtk

