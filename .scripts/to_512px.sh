#!/bin/env bash
#
# Converts images to 512x512 pixels using ffmpeg.
# Useful for making textures to upload to Second Life.

dest="./SL_512x512_Images"

if [[ ! -d "$dest" ]]; then
    notify-send "creating \"$dest\" folder"
    mkdir "$dest"
fi

# Pass files through ffmpeg and scale to new size, then save to dest output folder.
for file
    do
    if [ ! -e "$file" ]
        then
        continue
    fi
    to_name="$dest/${file%.*}.jpg"
    ffmpeg -i "${file}" -vf scale=512:512 "${to_name}"
done && notify-send "Image conversion completed"
