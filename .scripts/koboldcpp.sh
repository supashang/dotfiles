#!/bin/env bash
#
# Local AI Chat bot.
# Dependencies: koboldcpp, conda
#
#set -euo pipefail

# Path to koboldcpp
kbpath="$HOME/Projects/koboldcpp"
# Path to SillyTavern
stpath="$HOME/Projects/SillyTavern"
# AllTalkTTS path
atpath="$HOME/Projects/alltalk_tts"

# Find conda folder
if [[ ! -f "/home/$USER/miniconda3/etc/profile.d/conda.sh" ]]; then
    # If it doesn't exist - inform the user.
    notify-send "Path Error:" "It looks like conda is not installed. Exiting..."
    exit 1
else
    # Find kobold folder
    if [[ ! -d "$kbpath" ]]; then
        # If it doesn't exist - inform the user.
        notify-send "Path Error:" "Folder \"$kbpath\" not found."
    exit 1
    else
        # cd to the working path
        cd "$kbpath" || exit 2
        # Load koboldcpp
        echo "Loading koboldcpp..."
        python koboldcpp.py \
          --threads 4 \
          --contextsize 2048 \
          --smartcontext \
          --usecublas normal \
          --gpulayers 100 \
          --model "SFR-Iterative-DPO-LLaMA-3-8B-R-Q5_K_S.gguf" \
          --port 5002 &
       
        # AllTalkTTS
        if [[ ! -d $atpath ]]; then
          echo "No AllTalk path found"
          exit 2
        else
          # Load AlltalkTTS
          echo "Loading AllTalkTTS conda env..."
          source ~/miniconda3/etc/profile.d/conda.sh
          cd $atpath || exit 2
          conda activate alltalkenv
          ./start_alltalk.sh &
          
          echo "Loading AllTalkTTS..."
          sleep 15

          echo "Loading SillyTavern..."
          if [[ ! -d $stpath ]]; then
            echo "SillyTavern path not found!"
          else
            cd $stpath || exit 2
            ./start.sh
          fi
        fi
    fi
fi
