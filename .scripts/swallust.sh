#!/bin/env bash
#
# Name: swallust.sh
# Description: 
# Take an image as the input and display the image as a wallpaper using swww and sets colours using wallust.
# The script will also auto-enable swww-daemon if it's not running and exists on the system. 
# Usage: swallust.sh {filename}
# Creator: Supa Shang

check_running () {
if [ ! -f "/usr/bin/swww" ]; then
  notify-send "You need to install swww to use this script."
  exit 2
else 
  if command -v swww &>/dev/null; then 
    apps_to_check=("mpvpaper" "wpaperd" "swaybg" "hyprpaper" "variety") 
    for app in "${apps_to_check[@]}"; do 
      if pgrep -f "$app" > /dev/null; then 
        pkill "$app" 
      fi 
    done
  fi
  # start swww 
  if ! pidof swww-daemon > /dev/null; then 
    uwsm app -- swww-daemon --format xrgb 
    sleep 1 
  fi
fi
}

check_running &&
swww img --resize fit --transition-fps 60 --transition-type any --transition-duration 3 --transition-bezier 0.4,0.2,0.4,1.0 "$1" 
wallust run "$1"
echo "$1" > "$HOME/.cache/current_wallpaper"

