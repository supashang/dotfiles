#!/bin/env bash

# Check CPU usage.
# If cpuUsage is higher than cpuLimit then do option1 else do option2

cpuUsage=$(ps -A -o %cpu | awk '{s+=$1} END {print s}')
cpuLimit=20.0

if (( $(echo "$cpuUsage > $cpuLimit" | bc -l) )); then
    # option1
    echo "Is higher: ""$cpuUsage"
else
    #option2
    echo "Is lower: ""$cpuUsage"
fi
