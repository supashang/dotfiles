#!/bin/env bash
#
# Description: Rofi .scripts quick edit.
# Opens selected script in editor.
# Dependencies: rofi, exa
# Ensure $VISUAL variable is set in .profile

# Define .script path
scrpath="$HOME/.scripts/"

# Ensure .scripts folder exists
if [[ ! -d "$scrpath" ]]; then
    # If it doesn't, inform the user and exit.
    notify-send "\"$scrpath\" folder doesn't exist."
    exit 1
else
    # cd to the path provided above.
    cd "$scrpath" || exit 1
    # List all files in .scripts to Rofi
    scrp="$(exa -R "$scrpath" | rofi -dmenu -p "Edit Script")"

    # If user doesn't pick anything then exit here.
    if [[ -z "${scrp}" ]]; then
        exit 1
    else
        # Open our selected script.
        $VISUAL "$scrp"
    fi
fi
