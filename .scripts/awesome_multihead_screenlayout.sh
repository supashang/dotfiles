#!/bin/env bash
xrandr --output HDMI-0 --mode 1680x1050 --pos 0x0 --rotate normal --output DP-0 --primary --mode 2560x1440 --pos 1680x0 --rotate normal --output DP-1 --off --output DP-2 --off --output DP-3 --off --output DP-4 --off --output DP-5 --mode 1680x1050 --pos 4240x0 --rotate normal
