#!/bin/env bash
#                              _                   _            _
#  _ __ ___   ___  _   _ _ __ | |_      _ __  _ __(_)_   ____ _| |_ ___
# | '_ ` _ \ / _ \| | | | '_ \| __|____| '_ \| '__| \ \ / / _` | __/ _ \
# | | | | | | (_) | |_| | | | | ||_____| |_) | |  | |\ V / (_| | ||  __/
# |_| |_| |_|\___/ \__,_|_| |_|\__|    | .__/|_|  |_| \_/ \__,_|\__\___|
#                                      |_|
# by: Supa Shang
# Version: 0.40
# Dependencies: gocryptfs, gum, figlet, xdg-open
# Guide: https://spacebums.co.uk/post/gocryptfs/

set -e
clear

# check dependencies
programs=(gum gocryptfs figlet xdg-open)
for program in "${programs[@]}"; do
  if ! [ -x "/usr/bin/$program" ]; then
    echo "Would you like to install $program now?"
    # Prompt user to install the program and wait for input
    read -p "Install: $program ? (y/n) " response
    
    case $response in
      y)
        # Optional: Add code here to attempt to install the program
        yay -S "$program"
        clear
        ;;
      n)
        echo ""
        break
        ;;
      *)
        echo "Invalid choice!"
        exit
        ;;
    esac
  fi
done

# Set the paths to both your gocryptfs encrypted folders and mount folders below:
# Images
GOCRYPTFS_IMAGES="/media/Media/.images"
UNLOCKED_IMAGES="$HOME/Private/Images/"
# Docs
GOCRYPTFS_DOCUMENTS="/media/Media/.private"
UNLOCKED_DOCUMENTS="$HOME/Private/Docs/"
# Dropbox
GOCRYPTFS_DROPBOX="$HOME/Dropbox/.private"
UNLOCKED_DROPBOX="$HOME/Private/Dropbox/"

# Open file manager after mounting? true/false
FILEMANAGER=true

# Border colours
ERROR_COLOR="#f13e32"
BORDER_COLOR="#077600"

# Don't edit this
GCFS_MOUNT=""

# Check if mount parent folder exists
if [ ! -d "$HOME/Private/" ];then
    # Mount parent doesn't exist - pop a warning message.
    gum style \
    --border normal \
    --margin "1" \
    --padding "1" \
    --border-foreground $ERROR_COLOR \
    "🔒 Private Folders: NONE! 🔒
    ERROR: !! Please first create the folder $HOME/Private/ to use this script !!
    For a detailed guide on how to create gocryptfs folders,
    see here: https://spacebums.co.uk/post/gocryptfs/"
else
    # Mount parent exists - prompt user.
    gum style \
    --border normal \
    --margin "1" \
    --padding "1" \
    --border-foreground $BORDER_COLOR \
    " 🔒 Private Folders 🔒
    :: Will be mounted in $HOME/Private/
    :: Select a private folder to mount: "

    # Present choices to user
    choice=$(gum choose "Images" "Documents" "Dropbox" "Exit")
    if [ "$choice" != "" ];then
        # Perform actions
        if [ "$choice" == "Exit" ];then
            echo "Nothing selected: exited."
            sleep 3
            exit 1
        elif [ "$choice" == "Images" ];then
            if [ -d "$GOCRYPTFS_IMAGES" ];then
                figlet "Private Images"
                if [[ $(findmnt -M "$UNLOCKED_IMAGES") ]]; then
                    echo "$UNLOCKED_IMAGES is already mounted."
                    echo ":: Do you want to unmount it?"

                    umfs=$(gum choose "Yes" "No")
                    if [ "$umfs" == "Yes" ];then
                        fusermount -u "$UNLOCKED_IMAGES"
                    fi
                    exit 1
                fi
                echo "Enter your password to unlock."
                gocryptfs "$GOCRYPTFS_IMAGES" "$UNLOCKED_IMAGES"
                GCFS_MOUNT="$UNLOCKED_IMAGES"
            else
                echo "The folder $GOCRYPTFS_IMAGES does not exist!"
                exit 1
            fi
        elif [ "$choice" == "Documents" ];then
            if [ -d "$GOCRYPTFS_DOCUMENTS" ];then
                figlet "Private Documents"
                if [[ $(findmnt -M "$UNLOCKED_DOCUMENTS") ]]; then
                    echo "$UNLOCKED_DOCUMENTS is already mounted."
                    echo ":: Do you want to unmount it?"

                    umfs=$(gum choose "Yes" "No")
                    if [ "$umfs" == "Yes" ];then
                        fusermount -u "$UNLOCKED_DOCUMENTS"
                    fi
                    exit 1
                fi
                echo "Enter your password to unlock."
                gocryptfs "$GOCRYPTFS_DOCUMENTS" "$UNLOCKED_DOCUMENTS"
                GCFS_MOUNT="$UNLOCKED_DOCUMENTS"
            else
                echo "The folder $GOCRYPTFS_DOCUMENTS does not exist!"
                exit 1
            fi
        elif [ "$choice" == "Dropbox" ];then
            if [ -d "$GOCRYPTFS_DROPBOX" ];then
                figlet "Private Dropbox"
                if [[ $(findmnt -M "$UNLOCKED_DROPBOX") ]]; then
                    echo "$UNLOCKED_DROPBOX is already mounted."
                    echo ":: Do you want to unmount it?"

                    umfs=$(gum choose "Yes" "No")
                    if [ "$umfs" == "Yes" ];then
                        fusermount -u "$UNLOCKED_DROPBOX"
                    fi
                    exit 1
                fi
                echo "Enter your password to unlock."
                gocryptfs "$GOCRYPTFS_DROPBOX" "$UNLOCKED_DROPBOX"
                GCFS_MOUNT="$UNLOCKED_DROPBOX"
            else
                echo "The folder $GOCRYPTFS_DROPBOX does not exist!"
                exit 1
            fi
        fi
    fi

    sleep 2

    if [ "$FILEMANAGER" == "true" ]; then
        # Opening folder:
        uwsm app -- xdg-open "$GCFS_MOUNT"
    fi
fi
