#!/bin/env bash
#                             
# / ___|| | | |  _ \ / \      "rofi_sxhkdrc_keys.sh"
# \___ \| | | | |_) / _ \     "A script for viewing your sxhkd key bindings." 
#  ___) | |_| |  __/ ___ \    Supa Shang
# |____/ \___/|_| /_/   \_\   http://spacebums.co.uk
#                             Dependencies: sxhkd, dunstify, rofi
#
set -euo pipefail

# Filter text lines with awk, then pipe into column and rofi.
keys=$(awk '/^[A-z]/ && last {print $0,"\t",last} {last=""} /^#/{last=$0}' "$HOME/.config/sxhkd/sxhkdrc" | column -t -s $'\t' | rofi -dmenu -i -no-show-icons -theme-str 'window {width: 70%;}' -p "sxhkd Hotkeys")

# If user doesn't pick anything then exit here.
if [[ -z "${keys// }" ]]; then
    exit 1
fi

dunstify "$keys"
