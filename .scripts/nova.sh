#!/bin/env bash

# koboldcpp AI chat.
# Guide: https://spacebums.co.uk/ai-voice-chatting/

figlet "Nova AI chat"

# Projects Path. Where AI projects are stored.
projects_path="$HOME/Projects/"
# path for koboldcpp
kobolod_path="$projects_path/koboldcpp/"
# koboldcpp download URL
kobold_url="https://github.com/LostRuins/koboldcpp/releases/latest/download/koboldcpp-linux-x64-cuda1150"
# AllTalk path
alltalk_path="$projects_path/alltalk_tts/"
# Chat Model name:
chatmodel_name="L3-8B-Stheno-v3.2-Q5_K_S-imat.gguf"
# Chat model URL
model_url="https://huggingface.co/Lewdiculous/L3-8B-Stheno-v3.2-GGUF-IQ-Imatrix/resolve/main/L3-8B-Stheno-v3.2-Q5_K_S-imat.gguf"
# Whisper model name
whispermodel_name="whisper-base.en-q5_1.bin"
# Whisper model URL
whisper_url="https://huggingface.co/koboldcpp/whisper/resolve/main/whisper-base.en-q5_1.bin"

echo ":: Checking for CUDA..."
if [ -z "$CUDA_HOME" ]; then
    echo "CUDA is not installed, and the $CUDA_HOME is added to your PATH."
    echo "Would you like to install it? (recommended)"
    cuda_chk=$(gum choose "Yes" "No")

    if [ "$cuda_chk" == "Yes" ]; then
        yay -S cuda
    else
        echo "You need to install cuda to use the AllTalk_TTS features."
        sleep 5
        exit
    fi
else
    echo "CUDA installed."
fi

# Does miniconda folder exist?
if [[ ! -f "/home/$USER/miniconda3/etc/profile.d/conda.sh" ]]; then
    # If it doesn't exist - inform the user.
    echo "Path Error:" "It looks like conda is not installed. Exiting..."
    exit 1
fi


echo ":: Checking for Projects folder"
if [ ! -d "$projects_path" ]; then
    echo "Projects folder does not exist. Would you like to create it?"
    at_chk=$(gum choose "Yes" "No")
    if [ "$at_chk" == "Yes" ]; then
        mkdir -p "$projects_path"
    else
        echo "Exiting..."
        exit
    fi
else
    echo "Projects folder: ✅"
fi

echo ":: Checking for koboldcpp..."
if [ ! -f "$kobolod_path/koboldcpp" ]; then
    echo "koboldCpp is does not exist. Would you like to download it? (recommended)"
    kobold_chk=$(gum choose "Yes" "No")
    if [ "$kobold_chk" == "Yes" ]; then
        mkdir -p "$kobolod_path"
        echo "Downloading koboldcpp..."
        cd "$kobolod_path" || exit 1
        sleep 1;
        gum spin --spinner dot --title "downloading koboldcpp..." -- curl -fLo koboldcpp "$kobold_url"
        echo "done."
        chmod +x koboldcpp
    else
        echo "Exiting..."
        exit
    fi
else
    echo "koboldcpp exists: ✅"
fi

echo ":: Checking chat model exists"
if [ ! -f "$kobolod_path/$chatmodel_name" ]; then
    echo "Download chat model? (recommended)"
    chmdl=$(gum choose "Yes" "No")
    if [ "$chmdl" == "Yes" ]; then
        gum spin --spinner dot --title "downloading chat model..." -- wget "$model_url"
    else
        echo "You will need to download a chat model to use koboldCpp."
    fi
else
    echo "Chat model: ✅"
fi

echo ":: Checking whisper model exists"
if [ ! -f "$kobolod_path/$whispermodel_name" ]; then
    echo "Download whisper model file? This is needed to do voice to voice chat with the AI model. (recommended)"
    chwdl=$(gum choose "Yes" "No")
    if [ "$chwdl" == "Yes" ]; then
        cd "$kobolod_path" || exit
        gum spin --spinner dot --title "downloading whisper model..." -- wget "$whisper_url"
    else
        echo "skipping whisper model download."
    fi
else
    echo "Whisper model: ✅"
fi

# Run AllTalk_TTS
echo ":: Starting AllTalk"
cd "$alltalk_path" || exit
source ~/miniconda3/etc/profile.d/conda.sh
echo ":: Activating alltalk conda environment."
conda activate "$alltalk_path/alltalk_environment/env"
sleep 1
echo "Starting AllTalk"
./start_alltalk.sh &&

# Launch koboldCpp
cd "$kobolod_path" || exit
echo ":: Launching koboldCpp..."
./koboldcpp --config Nova.kcppy --launch && echo "Let's go!"
