#!/bin/env bash
#
# Description: uses yay to install / remove apps.
# Dependencies: yay, rofi, awk, sort, notify-send,

action=$(echo -e "Install App\nRemove App\nRemove Waste\nUpdate System\nExport Packages List" | rofi -dmenu -p "Software Manager: " -i)

case $action in 
  "Install App")
    package=$(yay -Qi |
      awk '/^Name/{name=$3} /^Download Size/{ds=$4$5} /^Repo/{repo=$3} /^Description/{sub(/^.{18}/,"", $0); print name, "["ds"]", "("repo") =>", $0} ' |
      sort -d | rofi -dmenu -i -l 20 -p "Install App"| awk '{print $1}')

    if [[ -z "${package// }" ]]; then
     exit 1
    fi

    yay -S "$package"
    notify-send "😎🎊 $package is installed ✨"
    ;;
  "Remove App")
    package=$(yay -Si |
      awk '/^Name/{name=$3} /^Installed Size/{ds=$4$5} /^Repo/{repo=$3} /^Description/{sub(/^.{18}/,"", $0); print name, "["ds"] =>", $0} ' |
      sort -d | rofi -dmenu -i -l 25 -p "Remove App"| awk '{print $1}')

    if [[ -z "${package// }" ]]; then
     exit 1
    fi

    yay -R "$package"
    notify-send "😈 $package is removed 😥"
    ;;
  "Remove Waste")
    yay -Rs "$(pacman -Qqtd)"
    notify-send "☠  Orphan packages deleted 😵"
    ;;
  "Export Packages List")
    cd || exit 1
    yay -Qqe > packages.txt
    notify-send "😵 Packages exported 😵"
    ;;
  "Update System")
    yay
    notify-send "✨🎉🥳 System Update Complete"
    ;;
  *)
    notify-send "😕 No Option Selected"
    ;;
esac
