#!/bin/env bash

if [[ "$1" == "logout" ]]; then
    echo ":: Exit"
    #sleep 0.5
    if [ "$XDG_SESSION_DESKTOP" == "Hyprland" ]; then
        uwsm stop
        #killall -9 Hyprland sleep 2
    elif [ "$DESKTOP_SESSION" == "river" ]; then
        riverctl exit
    fi
fi

if [[ "$1" == "lock" ]]; then
    echo ":: Lock"
    #sleep 0.5
    if [ "$XDG_SESSION_DESKTOP" == "Hyprland" ]; then
        hyprlock
    elif [ "$DESKTOP_SESSION" == "river" ]; then
        swaylock --color 000000 --image "$HOME/Pictures/lockscreen/i21i0g90drb91.jpg"
    fi
fi

if [[ "$1" == "reboot" ]]; then
    echo ":: Reboot"
    #sleep 0.5
    systemctl reboot
fi

if [[ "$1" == "shutdown" ]]; then
    echo ":: Shutdown"
    #sleep 0.5
    systemctl poweroff
fi

if [[ "$1" == "suspend" ]]; then
    echo ":: Suspend"
    #sleep 0.5
    systemctl suspend    
fi

if [[ "$1" == "hibernate" ]]; then
    echo ":: Hibernate"
    #sleep 1;
    systemctl hibernate    
fi
