#!/bin/env bash
# "Supa's Skyrim / Mod Organizer Run Script". v0.00.035 Last Updated on 04/02/2022.
# This script is for use with the original "The Elder Scrolls V: Skyrim" only, not the later "The Elder Scrolls V: Skyrim Special Edition".
# *IMPORTANT* leave WINEESYNC=0 to the stop game heavy stuttering when moving your mouse.
steam_client="$HOME/.steam/steam"
# Replace with the path to your Skyrim game folder if it isn't in the default Steam library install location.
SKYRIM_INSTALL_PATH="/media/Speedy/SteamLibrary/steamapps/common/Skyrim/"
# The path to the ModOrganizer executable.
MOD_ORGANIZER_EXECUTABLE="/media/Speedy/Skyrim_Mod_Tools/ModOrganizer/ModOrganizer.exe"

#MOD_ORGANIZER_EXECUTABLE="/media/Speedy/Games/Skyrim_Mod_Tools/Mod.Organizer-2.4.4/ModOrganizer.exe"
# Start Steam as background process and allow Mod Organizer to run after it.
nohup steam -tcp >/dev/null 2>&1 &
# Notify the user as to what is happening.
notify-send -i "/usr/share/icons/gnome/22x22/categories/applications-games.png" "Loading Steam & Mod Organizer..."
# Sleep for a few seconds while Steam loads.
sleep 10
# Fix Skyrim sticky keys problem
xset -r
# cd into the Skyrim installation path and run the command declared earlier in DEF_CMD variable.
cd $SKYRIM_INSTALL_PATH || exit
DEF_CMD=$MOD_ORGANIZER_EXECUTABLE
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Copy/paste PATH from /tmp/proton_{user}/run
# Don't copy and paste this whole script and expect it to run on your system, as the paths below are for my system only.
# Generate your own run script as shown in my online guide: https://spacebums.co.uk/post/tes_skyrim_modding_guide_linux/#skyrim-launch-options

PATH="/media/Speedy/SteamLibrary/steamapps/common/Proton 4.11/dist/bin/:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/amd64/bin:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/amd64/usr/bin:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/supa/.local/bin:/home/supa/AppImages:/home/supa/.scripts:/home/supa/.config/emacs/bin/" \
	TERM="xterm" \
	WINEDEBUG="-all" \
	WINEDLLPATH="/media/Speedy/SteamLibrary/steamapps/common/Proton 4.11/dist/lib64//wine:/media/Speedy/SteamLibrary/steamapps/common/Proton 4.11/dist/lib//wine" \
	LD_LIBRARY_PATH="/media/Speedy/SteamLibrary/steamapps/common/Proton 4.11/dist/lib64/:/media/Speedy/SteamLibrary/steamapps/common/Proton 4.11/dist/lib/:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/pinned_libs_32:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/pinned_libs_64:/opt/intel/oneapi/compiler/latest/linux/lib:/opt/intel/oneapi/compiler/latest/linux/compiler/lib/intel64_lin:/opt/intel/oneapi/tbb/latest/lib/intel64/gcc4.8:/usr/lib32:/usr/lib/opencollada:/usr/lib/libfakeroot:/usr/lib:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/lib/i386-linux-gnu:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/usr/lib/i386-linux-gnu:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/lib/x86_64-linux-gnu:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/usr/lib/x86_64-linux-gnu:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/lib:/home/supa/.local/share/Steam/ubuntu12_32/steam-runtime/usr/lib:/media/Speedy/SteamLibrary/steamapps/common/Skyrim" \
	WINEPREFIX="/media/Speedy/SteamLibrary/steamapps/compatdata/72850/pfx/" \
	WINEESYNC="1" \
	SteamGameId="72850" \
	SteamAppId="72850" \
	WINEDLLOVERRIDES="steam.exe=b;mfplay=n;dxvk_config=n;d3d11=n;d3d10=n;d3d10core=n;d3d10_1=n" \
	STEAM_COMPAT_CLIENT_INSTALL_PATH="/home/supa/.local/share/Steam" \
	"/media/Speedy/SteamLibrary/steamapps/common/Proton 4.11/dist/bin/wine" steam.exe "${@:-${DEF_CMD[@]}}"

# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# When finished playing, set keys back to normal by exiting Mod Organizer and closing Steam maunally.
# Note: This will stay unset if you close the terminal, so close Mod Organizer first before you close the terminal after playing.
# Restore normal key settings
xset r
# Exit blarb.
notify-send -i "/usr/share/icons/gnome/22x22/categories/applications-games.png" "Keyboard settings restored."
echo "I used to be a Linux adventurer like you, but I took an arrow to the knee!"
# Kill Skyrim if it hangs
#ps -ef | grep TESV | grep -v grep | awk '{print $2}' | xargs kill
# Done
