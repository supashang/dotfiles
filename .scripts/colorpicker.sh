#!/bin/env bash

# Left-click on a color on screen to copy color data to clipbopard.
# Needs colorpicker to be installed to work.

colorpicker --short --one-shot --preview | xclip -selection clipboard
