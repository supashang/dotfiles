#!/bin/env bash

set -euo pipefail

text="This is some test text."

if [ -n "$text" ]; then
  for word in ${text// / } ; do
    printf "%s " "$(tput setaf 3)$word"
    aplay -q "$HOME/.scripts/sfx/bloop2.wav" 2>/dev/null &
    sleep 0.05
  done
  tput sgr0
  printf "\n"
fi
