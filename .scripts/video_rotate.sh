#!/bin/env bash
#
# Rotates video files 90 degrees.
# Useful for flipping videos filmed on an old phone.
#

set -euo pipefail

dest="./Converted_Videos"

if [[ ! -d "$dest" ]]; then
    notify-send "creating \"$dest\" folder"
    mkdir "$dest"
fi

for file
    do
    if [ ! -e "$file" ]
        then
        continue
    fi
    notify-send "Processing: \"$file\" "
    to_name="$dest/${file%.*}.m4v"
    ffmpeg -i "${file}" -vf rotate=-PI/2:720:1280 "${to_name}"
done
