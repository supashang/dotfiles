#!/bin/env bash
# Screenshot using pypr scratchpad
# by Supa Shang
# Description: Prompts user with options to take image or video snapshots of a screen.
# Dependecies: hyprshot, slurp, rofi, wf-recorder, swappy, xclip, thunar, gum, pypr

# Path to save screenshots.
image_dir="$HOME/Pictures/screenshots"
# Share screenshots to a friend in shared Dropbox folder.
share_dir="$HOME/Dropbox/Sleezy_Shared/screenshots"
# Create filename with date.
image_name="screenshot_$(date +%d%m%Y_%H%M%S).png"
# Screen to capture video from.
vid_screen="DP-1"
# Set a delay before capture for Area Only
delay=5
# delay to wait for pypr scratchpad to close
delay_pypr=0.5
# Kill video screen recorder if recording.
wf_rec="$(pgrep wf-recorder)"
if [ -n "$wf_rec" ]; then
    kill "$wf_rec"
    notify-send "Stopped Video Recording."
    exit
fi

printf "
███████╗ ██████╗██████╗ ███████╗███████╗███╗   ██╗███████╗██╗  ██╗ ██████╗ ████████╗
██╔════╝██╔════╝██╔══██╗██╔════╝██╔════╝████╗  ██║██╔════╝██║  ██║██╔═══██╗╚══██╔══╝
███████╗██║     ██████╔╝█████╗  █████╗  ██╔██╗ ██║███████╗███████║██║   ██║   ██║
╚════██║██║     ██╔══██╗██╔══╝  ██╔══╝  ██║╚██╗██║╚════██║██╔══██║██║   ██║   ██║
███████║╚██████╗██║  ██║███████╗███████╗██║ ╚████║███████║██║  ██║╚██████╔╝   ██║
╚══════╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝ ╚═════╝    ╚═╝
"

echo "Please select a screenshot option."
ps_choice=$(gum choose "Image - Fullscreen" "Image - Window" "Image - Area" "Image - Area 5s delay" "Image - Dropbox" "Image - Scribble" "Image - Clipboard" "Video - Record Fullscreen no sound" "Video - Record Fullscreen with sound" "Video - Record Region no sound" "Video - Record Region with sound" "Video - File Browse")

if [ "$ps_choice" == "Exit" ]; then
    pypr toggle printscreen
    exit
elif [ "$ps_choice" == "Image - Area" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    hyprshot -z -m region -o "$image_dir"
elif [ "$ps_choice" == "Image - Area 5s delay" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    if [ "$delay" -gt 0 ];then
        notify-send "Area capture in 5 seconds..."
        sleep "$delay"
    fi
    hyprshot -z -m region -o "$image_dir"
elif [ "$ps_choice" == "Image - Fullscreen" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    hyprshot -z -m output -o "$image_dir"
elif [ "$ps_choice" == "Image - Window" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    hyprshot -z -m window -o "$image_dir"
elif [ "$ps_choice" == "Image - Dropbox" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
     hyprshot -z -m output -o "$share_dir"
elif [ "$ps_choice" == "Image - Scribble" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    grim -g "$(slurp)" "$image_dir/$image_name"
    xclip -selection clipboard -t image/png -i "$image_dir/$image_name"
    notify-send "Screenshot saved and copied to clipboard" "Mode: Scribble"
    swappy -f "$image_dir/$image_name"
elif [ "$ps_choice" == "Image - Clipboard" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    hyprshot -z -m output --clipboard-only
elif [ "$ps_choice" == "Video - Record Fullscreen no sound" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    wf-recorder -o "$vid_screen" -f "$HOME/Videos/$(whoami)_screenclip_$(date +%Y_%m_%d_%H-%M-%S).mp4"
elif [ "$ps_choice" == "Video - Record Fullscreen with sound" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    nohup wf-recorder -a -o "$vid_screen" -f "$HOME/Videos/$(whoami)_screenclip_$(date +%Y_%m_%d_%H-%M-%S).mp4" > /dev/null 2>&1 & 
elif [ "$ps_choice" == "Video - Record Region no sound" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    wf-recorder -g "$(slurp)" -o "$vid_screen" -f "$HOME/Videos/$(whoami)_screenclip_$(date +%Y_%m_%d_%H-%M-%S).mp4"
elif [ "$ps_choice" == "Video - Record Region with sound" ]; then
    pypr toggle printscreen
    sleep "$delay_pypr"
    wf-recorder -a -g "$(slurp)" -o "$vid_screen" -f "$HOME/Videos/$(whoami)_screenclip_$(date +%Y_%m_%d_%H-%M-%S).mp4"
elif [ "$ps_choice" ==  "Video - File Browse" ]; then
    uwsm app -- thunar "$HOME/Videos/"
fi

# Don't remove this
sleep 2
