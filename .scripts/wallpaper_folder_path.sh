#!/bin/env bash
#                _ _                                __       _     _
# __      ____ _| | |_ __   __ _ _ __   ___ _ __   / _| ___ | | __| | ___ _ __
# \ \ /\ / / _` | | | '_ \ / _` | '_ \ / _ \ '__| | |_ / _ \| |/ _` |/ _ \ '__|
#  \ V  V / (_| | | | |_) | (_| | |_) |  __/ |    |  _| (_) | | (_| |  __/ |
#   \_/\_/ \__,_|_|_| .__/ \__,_| .__/ \___|_|    |_|  \___/|_|\__,_|\___|_|
#                   |_|         |_|
#
# Set wallpaper theme path.
# by: Supa Shang
# Description: Allows the user to set the wallapaper folder via Rofi.
# The output file can then be used by other scripts to search for wallpapers contained in that folder.
#
# e,g. ~/.scripts/wallpaper_random.sh
#
#set -x

# Set path to wallpaper main folder here:
wallpapers="$HOME/Pictures/wallpaper"
# How deep do you want to search for subfolders in $wallpapers (1 - 3)
subfolder_depth=3
# The file to save the path of the wallpaper folder to. (Don't edit this!):
wallpaper_theme_path="$HOME/.cache/wallpaper_theme_path"

if [ ! -e "$wallpapers" ]; then
notify-send "ERROR: $wallpapers not found!"
    exit 1
else
    ## List only directories into Rofi using fd.
    selected_folder=$(fd . "$wallpapers" -t d -L -d $subfolder_depth | rofi -dmenu -p "Random Wallpaper Folder")
    if [ -n "$selected_folder" ]; then
      echo "$selected_folder" > "$wallpaper_theme_path"
      notify-send -i "/usr/share/icons/gnome/24x24/apps/preferences-desktop-wallpaper.png" "Wallpaper folder set:" "$selected_folder"
    else
      exit 1
    fi

    # Check if wpaperd PID is running and config file exists.
    wp_pid=$(pidof wpaperd)
    wp_config="$HOME/.config/wpaperd/config.toml"
    if [ -f "$wp_config" ] && [ -n "$wp_pid" ]; then
       # Replace the path in the config with our selected path.
       sed -i "s|path = .*|path = \"${selected_folder}\"|" "$wp_config"
       # Kill existing wpaperd PID and restart so it reads the new wallpaper path from its config.
       kill -9 "$wp_pid"
       sleep 1
       if [ "$XDG_SESSION_DESKTOP" == "Hyprland" ]; then
         uwsm app -- wpaperd -d &
       elif [ "$DEKSTOP_SESSION"  == "hyprland" ]; then
         wpaperd -d &
       fi
    fi

    # Set random wallpaper.
    wallpaper_random.sh
fi
