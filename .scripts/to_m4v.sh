#!/bin/env bash
# Converts video files to 720p m4v

set -euo pipefail

# Folder name of where converted videos will be stored.
dest="./m4v_videos"

# If the folder doesn't exist - create it.
if [[ ! -d "$dest" ]]; then
    notify-send "creating \"$dest\" folder"
    mkdir "$dest"
fi

for file
    do
    if [ ! -e "$file" ]
        then
        continue
    fi
    notify-send "Processing: \"$file\" "
    to_name="$dest/${file%.*}.m4v"
    #ffmpeg -i "${file}" -c:v libx264 -crf 18 -vf format=yuv420p -c:a copy "${to_name}"
    #ffmpeg -i "${file}" -codec copy "${to_name}"
    ffmpeg -i "${file}" -s hd720 -c:v h264_nvenc -crf 23 -c:a aac -strict -2 "${to_name}"
done && notify-send "Video conversion complete."
