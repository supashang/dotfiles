#!/bin/env bash
# Scratchpad script.
# Credits: Jake@Linux (https://www.youtube.com/watch?v=su5cqjVYFcs)

# set -euo pipefail
# Used with AwesomeWM

case $1 in
    ranger)
        entry="kitty -T ranger -e ranger"
        ;;
    pulsemixer)
        entry="kitty -T pulsemixer -e pulsemixer"
        ;;
    music)
        entry="kitty -T music -e ncmpcpp"
esac

xdotool search --onlyvisible --name "$1" windowunmap \
    || xdotool search --name "$1" windowmap \
    || $entry &
