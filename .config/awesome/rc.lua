package.loaded["awful.hotkeys_popup.keys.tmux"] = {}

package.loaded["naughty.dbus"] = {} -- Dunst hack

local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local ipairs, string, os, table, tostring, tonumber, type = ipairs, string, os, table, tostring, tonumber, type

local gears         = require("gears")
local awful         = require("awful")
                      require("awful.autofocus")
local wibox         = require("wibox")
local beautiful     = require("beautiful")
--local naughty       = require("naughty") -- Disabled as we use dunst
local lain          = require("lain")
local menubar       = require("menubar")
--local freedesktop   = require("freedesktop") -- Disabled as slows awesome
local hotkeys_popup = require("awful.hotkeys_popup").widget
                      require("awful.hotkeys_popup.keys")
local my_table      = awful.util.table or gears.table -- 4.{0,1} compatibility
local dpi           = require("beautiful.xresources").apply_dpi

local logout_popup = require("awesome-wm-widgets.logout-popup-widget.logout-popup")

-- Scratchpad
--local scratch = require("scratch")

screen_width = awful.screen.focused().geometry.width
screen_height = awful.screen.focused().geometry.height

if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     font = "Hack Nerd Font 18",
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end
-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         font = "Hack Nerd Font 18",
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end

local themes = {
    "neon-candy", --1
    "spacebums-gruv", --2
}

local chosen_theme = themes[2]
local altkey       = "Mod1"
local super        = "Mod4"
local terminal     = os.getenv("TERMINAL") or "kitty"
local vi_focus     = false -- vi-like client focus - https://github.com/lcpz/awesome-copycats/issues/275
local cycle_prev   = true -- cycle trough all previous client or just the first -- https://github.com/lcpz/awesome-copycats/issues/274
local editor       = os.getenv("EDITOR") or "nvim"
local maxtags      = 9
--local gui_editor   = os.getenv("GUI_EDITOR") or "subl" -- Unused as we use sxhkd
--local browser      = os.getenv("BROWSER") or "brave"   -- (as above)

awful.util.terminal = terminal
awful.util.tagnames = { "1", "2", "3", "4", "5", "6", "7", "8", "9"}
awful.layout.layouts = {
awful.layout.suit.tile,
--awful.layout.suit.tile.left,
--awful.layout.suit.tile.bottom,
awful.layout.suit.tile.top,
awful.layout.suit.fair,
--awful.layout.suit.fair.horizontal,
--awful.layout.suit.floating,
--awful.layout.suit.spiral,
--awful.layout.suit.spiral.dwindle,
--awful.layout.suit.max,
--awful.layout.suit.max.fullscreen,
--awful.layout.suit.magnifier,
--awful.layout.suit.corner.nw,
--awful.layout.suit.corner.ne,
--awful.layout.suit.corner.sw,
--awful.layout.suit.corner.se,
--lain.layout.cascade,
--lain.layout.cascade.tile,
--lain.layout.centerwork,
--lain.layout.centerwork.horizontal,
--lain.layout.termfair,
--lain.layout.termfair.center
}

awful.util.taglist_buttons = my_table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ super }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ super }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

awful.util.tasklist_buttons = my_table.join(
    awful.button({ }, 1, function (c)
        if c == client.focus then
            c.minimized = true
        else
            c.minimized = false
            if not c:isvisible() and c.first_tag then
                c.first_tag:view_only()
            end
            client.focus = c
            c:raise()
        end
    end),
    awful.button({ }, 2, function (c) c:kill() end),
    awful.button({ }, 3, function ()
        local instance = nil

        return function ()
            if instance and instance.wibox.visible then
                instance:hide()
                instance = nil
            else
                instance = awful.menu.clients({theme = {width = dpi(250)}})
            end
        end
    end),
    awful.button({ }, 4, function () awful.client.focus.byidx(1) end),
    awful.button({ }, 5, function () awful.client.focus.byidx(-1) end)
)

lain.layout.termfair.nmaster           = 3
lain.layout.termfair.ncol              = 1
lain.layout.termfair.center.nmaster    = 3
lain.layout.termfair.center.ncol       = 1
lain.layout.cascade.tile.offset_x      = dpi(2)
lain.layout.cascade.tile.offset_y      = dpi(32)
lain.layout.cascade.tile.extra_padding = dpi(5)
lain.layout.cascade.tile.nmaster       = 5
lain.layout.cascade.tile.ncol          = 2

beautiful.init(string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), chosen_theme))

screen.connect_signal("property::geometry", function(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end)

screen.connect_signal("arrange", function (s)
    local only_one = #s.tiled_clients == 1
    for _, c in pairs(s.clients) do
        if only_one and not c.floating or c.maximized then
            c.border_width = 0
        else
            c.border_width = beautiful.border_width
        end
    end
end)

awful.screen.connect_for_each_screen(function(s)
        beautiful.at_screen_connect(s)

        -- Show/hide systray feature
        s.systray = wibox.widget.systray()
        s.systray.visible = false
end)

root.buttons(my_table.join(
    --awful.button({ }, 3, function () awful.util.mymainmenu:toggle() end), -- disable RMB menu
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)))

globalkeys = my_table.join(

-- Show/Hide systray widgets
awful.key({ super }, "u", function () awful.screen.focused().systray.visible = not awful.screen.focused().systray.visible end,
    {description = "Toggle systray visibility", group = "custom"}),

--Logout popout widget
awful.key({ super }, "l", function () logout_popup.launch() end,
  {description = "Show logout widget", group = "custom"}),

--Fallback terminal
awful.key({ super, altkey     }, "Return", function () awful.spawn(terminal) end,
  {description = "open a terminal", group = "launcher"}),

-- Window resize
awful.key({ super, "Shift"    }, "Right",     function () awful.tag.incmwfact( 0.01) end,
  {description = "resize window right", group = "client"}),
awful.key({ super, "Shift"    }, "Left",     function () awful.tag.incmwfact(-0.01) end,
  {description = "resize window left", group = "client"}),
awful.key({ super, "Shift"    }, "Down",     function () awful.client.incwfact( 0.01) end,
  {description = "resize window down", group = "client"}),
awful.key({ super, "Shift"    }, "Up",     function () awful.client.incwfact(-0.01) end,
  {description = "resize window up", group = "client"}),

-- Move client to tag and switch.
awful.key({ super, altkey }, "Left",
  function ()
    -- get current tag
    local t = client.focus and client.focus.first_tag or nil
    if t == nil then
        return
    end
    -- get previous tag (modulo 9 excluding 0 to wrap from 1 to 9)
    local tag = client.focus.screen.tags[(t.name - 2) % maxtags + 1]
    awful.client.movetotag(tag)
    awful.tag.viewprev()
  end,
    {description = "move client previous tag ", group = "client"}),

awful.key({ super, altkey }, "Right",
  function ()
    -- get current tag
    local t = client.focus and client.focus.first_tag or nil
    if t == nil then
        return
    end
    -- get next tag (modulo 9 excluding 0 to wrap from 9 to 1)
    local tag = client.focus.screen.tags[(t.name % maxtags) + 1]
    awful.client.movetotag(tag)
    awful.tag.viewnext()
  end,
    {description = "move client next tag", group = "client"}),

-- Hotkey Helper
awful.key({ super,           }, "s", function () local geo = screen[1].geometry hotkeys_popup.new{width = 1000, height = 800}:show_help() end,
  {description = "show help", group="awesome"}),
-- Tag browsing
awful.key({ super,           }, "Left",   awful.tag.viewprev,
  {description = "view previous", group = "tag"}),
awful.key({ super,           }, "Right",  awful.tag.viewnext,
  {description = "view next", group = "tag"}),
awful.key({ super,           }, "Escape", awful.tag.history.restore,
  {description = "go back", group = "tag"}),
-- Non-empty tag browsing
-- awful.key({ altkey }, "Left", function () lain.util.tag_view_nonempty(-1) end,
--   {description = "view previous nonempty", group = "tag"}),
-- awful.key({ altkey }, "Right", function () lain.util.tag_view_nonempty(1) end,
--   {description = "view previous nonempty", group = "tag"}),
-- Scratchpad

-- awful.key({ super,           }, "v", function () scratch.toggle("urxvt -name scratch -e pulsemixer", { instance = "scratch" }) end,
--   {description = "scratchpad", group = "client"}),

--[[
-- Show Main Menu : Disabled
awful.key({ super,           }, "w", function () awful.util.mymainmenu:show() end,
    {description = "show main menu", group = "awesome"}),
--]]

-- Layout manipulation
awful.key({ super, "Shift"   }, "j", function () awful.client.swap.byidx(  1) end,
    {description = "swap with next client", group = "client"}),
awful.key({ super, "Shift"   }, "k", function () awful.client.swap.byidx( -1) end,
    {description = "swap with previous client", group = "client"}),
-- Screen Focus
awful.key({ super, "Control" }, "Left", function () awful.screen.focus_relative(-1) end,
    {description = "focus the previous screen", group = "screen"}),
awful.key({ super, "Control" }, "Right", function () awful.screen.focus_relative( 1) end,
    {description = "focus the next screen", group = "screen"}),
-- Show/Hide Wibox
awful.key({ super }, "b", function () for s in screen do s.mywibox.visible = not s.mywibox.visible
    if s.mybottomwibox then s.mybottomwibox.visible = not s.mybottomwibox.visible end end end,
    {description = "wibox toggle", group = "awesome"}),
-- On the fly useless gaps change
awful.key({ super }, "=", function () lain.util.useless_gaps_resize(1) end,
    {description = "gaps increase", group = "client"}),
awful.key({ super }, "-", function () lain.util.useless_gaps_resize(-1) end,
    {description = "gaps decrease", group = "client"}),
-- Cycle active window
awful.key({ super, "Shift"   }, "Tab",
    function ()
        if cycle_prev then
            awful.client.focus.byidx(1)
            if client.focus then
                client.focus:raise()
            end
        end
    end,
    {description = "go forth", group = "client"}),

-- =========================================
-- SESSION
-- =========================================

-- Restart Awesome
awful.key({ super, "Control" }, "r", awesome.restart,
    {description = "reload awesome", group = "awesome"}),
-- Quit Awesome
awful.key({ super, "Shift"   }, "q", awesome.quit,
    {description = "quit awesome", group = "awesome"}),

-- =========================================
-- LAYOUT SELECTION
-- =========================================

-- Select next tiling layout
awful.key({ super, "Shift" }, "space", function() awful.layout.inc(1) end,
    {description = "select next tiling layout", group = "layout"})
)

-- ####################
-- Screen Actions
-- ####################

clientkeys = my_table.join(
--awful.key({ super, "Shift"   }, "m",      lain.util.magnify_client,
--    {description = "magnify client", group = "client"}),

-- Toggle titlebars
awful.key({ super, "Shift" }, 't', function (c) awful.titlebar.toggle(c) end,
    {description = 'toggle title bar', group = 'client'}),

awful.key({ super,           }, "f",  awful.client.floating.toggle,
    {description = "floating (toggle)", group = "client"}),

awful.key({ super, "Shift" }, "f", function (c) c.fullscreen = not c.fullscreen c:raise() end,
    {description = "fullscreen (toggle)", group = "client"}),

awful.key({ super, "Shift"   }, "o", function (c) c:move_to_screen() end,
    {description = "move to screen", group = "client"}),

-- awful.key({ super,           }, "n", function (c) c.minimized = true end,
--     {description = "minimize", group = "client"}),

-- awful.key({ super, "Shift"   }, "n",   function () local c = awful.client.restore() if c then client.focus = c c:raise() end end,
--     {description = "restore minimized", group = "client"}),

-- awful.key({ super, "Shift"   }, "m", function (c) c.maximized = not c.maximized c:raise() end,
--     {description = "maximize", group = "client"}),

awful.key({ super,           }, "x", function (c) c:kill() end,
    {description = "close", group = "client"})
)

-- Bind all key numbers to tags.
for i = 1, 9 do
    -- Hack to only show tags 1 and maxtags in the shortcut window (mod+s)
    local descr_view, descr_toggle, descr_move, descr_toggle_focus
    if i == 1 or i == 9 then
        descr_view = {description = "view tag #", group = "tag"}
        descr_toggle = {description = "toggle tag #", group = "tag"}
        descr_move = {description = "move focused client to tag #", group = "tag"}
        descr_toggle_focus = {description = "toggle focused client on tag #", group = "tag"}
    end
    globalkeys = my_table.join(globalkeys,
        -- View tag only.
        awful.key({ super }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  descr_view),
        -- Toggle tag display.
        awful.key({ super, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  descr_toggle),
        -- Move client to tag.
        awful.key({ super, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  descr_move),
        -- Toggle tag on focused client.
        awful.key({ super, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  descr_toggle_focus)
    )
end

clientbuttons = gears.table.join(
awful.button({ }, 1, function (c)
    c:emit_signal("request::activate", "mouse_click", {raise = true}) end),
awful.button({ super }, 1, function (c)
    c:emit_signal("request::activate", "mouse_click", {raise = true})
    awful.mouse.client.move(c) end),
awful.button({ super }, 3, function (c)
    c:emit_signal("request::activate", "mouse_click", {raise = true})
    awful.mouse.client.resize(c) end)
)

root.keys(globalkeys)

-- Rules to apply to new clients (through the "manage" signal).

-- All clients will match this rule.
awful.rules.rules = {
    {
      rule = { },
      properties = {
        border_width = beautiful.border_width,
        border_color = beautiful.border_normal,
        focus = awful.client.focus.filter,
        raise = true,
        keys = clientkeys,
        buttons = clientbuttons,
      --screen = mouse.screen,
        screen = awful.screen.preferred,
        placement = awful.placement.no_overlap+awful.placement.no_offscreen,
        switch_to_tags = true,
        size_hints_honor = false }
    },

-- Scratchpad rxvt-unicode
    {
      rule_any = {
        instance = { "scratch" },
        class = { "scratch" },
        icon_name = { "scratchpad_urxvt" },
      },
      properties = {
        skip_taskbar = true,
        floating = true,
        ontop = true,
        minimized = true, -- no need to minimize on load
        sticky = false,
        width = screen_width * 0.7,
        height = screen_height * 0.6,
      },
      callback = function(c)
        awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
        gears.timer.delayed_call(function()
            c.urgent = false
        end)
      end
    },

-- Scratchpad sxhkd rule
    {
      rule_any = { name = {"ranger", "pulsemixer" }},
      properties = {
        skip_taskbar = true,
        --ontop = true,
        floating = true,
        width = screen_width * 0.7,
        height = screen_height * 0.7,
        --callback = function(c) awful.placement.centered(c) end, -- old code (do not enable)
      },
      callback = function(c)
        awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
        gears.timer.delayed_call(function()
            c.urgent = false
        end)
      end
    },

-- Appear floating and centered (class)
    {
      rule_any = { class = {
                     "copyq", "Galculator", "Variety", "Engrampa", "Mugshot","Arandr", "Gcolor3","Gcolor2",
                     "flameshot", "Xfce4-about", "Kvantum Manager","Lxappearance","ClipGrab","qt5ct","ksnip","Qalculate-gtk"
                 }},
      --except_any = { name = {} },
      properties = {
        --width = screen_width * 0.7,
        --height = screen_height * 0.6,
        ontop = true,
        floating = true,
        --tag = screen[2].tags[1],
        callback = function(c) awful.placement.centered(c) end,}
    },

-- sxiv window appears floating and centered
    {
      rule_any = { name = {"sxiv","feh"} },
      properties = {
        width = screen_width * 0.8,
        height = screen_height * 0.8,
        ontop = true,
        --floating = true,
        callback = function(c) awful.placement.centered(c) end,}
    },

-- Telegram & Mumble floating on right side.
    {
      rule_any = { class = {"Mumble", "TelegramDesktop",} },
      --except_any = { name = {} },
      properties = {
        width = screen_width * 0.3,
        height = screen_height * 0.6,
        floating = true,
        --ontop    = true,
        --tag = screen[0].tags[1],
        --titlebars_enabled = false,
        callback = function(c) awful.placement.right(c) end,}
    },

-- Steam Friends floating on right side.
    {
      rule_any = { name = {"Friends List"} },
      properties = {
        width = screen_width * 0.2,
        --height = screen_height * 0.5,
        floating = true,
        ontop    = true,
        --tag = screen[0].tags[1],
        --titlebars_enabled = false,
        callback = function(c) awful.placement.right(c) end,}
    },

-- Pause mpc audio if mpv launched
    {
      rule = { class = "mpv" },
      properties = {
        width = screen_width * 0.5,
        height = screen_height * 0.5,
        floating = true,
        ontop    = true,
        callback = function(c) awful.spawn('mpc pause') end,}
    },

-- Make all dialog boxes floaty and be centered.
    {
      rule_any = { type = {"dialog" } },
      properties = {
        ontop = true,
        floating = true,
        callback = function(c) awful.placement.centered(c) end,}
    },

-- Titlebars
    {
      rule_any = { type = { "normal" } },
      properties = {
        titlebars_enabled = false }
    },

}

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- Custom
    if beautiful.titlebar_fun then
        beautiful.titlebar_fun(c)
        return
    end

    -- Default
    -- buttons for the titlebar
    local buttons = my_table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 2, function() c:kill() end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c, {size = dpi(16)}) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

--Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
c:emit_signal("request::activate", "mouse_enter", {raise = vi_focus}) end)
client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- Garbage collection
require("gears").timer {
  timeout = 5,
  autostart = true,
  call_now = true,
  callback = function()
    collectgarbage "collect"
  end,
}

collectgarbage("setpause", 110)
collectgarbage("setstepmul", 1000)

awful.spawn.with_shell("~/.scripts/awesome_autorun.sh")
