--[[
    ____  ____   _    ____ _____ ____  _   _ __  __ ____         ____ ____  _   ___     __
   / ___||  _ \ / \  / ___| ____| __ )| | | |  \/  / ___|       / ___|  _ \| | | \ \   / /
   \___ \| |_) / _ \| |   |  _| |  _ \| | | | |\/| \___ \ _____| |  _| |_) | | | |\ \ / /
    ___) |  __/ ___ \ |___| |___| |_) | |_| | |  | |___) |_____| |_| |  _ <| |_| | \ V /
   |____/|_| /_/   \_\____|_____|____/ \___/|_|  |_|____/       \____|_| \_\\___/   \_/

    theme:           "spacebums-gruv"
    by:              Supa Shang
    modified from:   "Powerarrow Dark" Awesome WM theme (github.com/lcpz)

----]]

local os = os
local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi
local my_table = awful.util.table or gears.table
local markup = lain.util.markup
local separators = lain.util.separators

-- Gruvbox Dark Palette
bg      = "#282828"
red     = "#cc241d"
green   = "#98971a"
yellow  = "#d79921"
blue    = "#458588"
purple  = "#b16286"
aqua    = "#689d6a"
gray    = "#a89984"

gray2   = "#928374"
red2    = "#fb4934"
green2  = "#b8bb26"
yellow2 = "#fabd2f"
blue2   = "#83a598"
purple2 = "#d3869b"
aqua2   = "#8ec07c"
fg      = "#ebdbb2"

bg0_h   = "#1d2021"
bg0     = "#282828"
bg1     = "#3c3836"
bg2     = "#504945"
bg3     = "#665c54"
bg4     = "#7c6f64"
gray3   = "#928374"
orange  = "#d65d0e"

bg0_s   = "#32302f"
fg4     = "#a89984"
fg3     = "#bdae93"
fg2     = "#d5c4a1"
fg1     = "#ebdbb2"
fg0     = "#fbf1c7"
orange2 = "#fe8019"

local theme                                     = {}
theme.dir                                       = os.getenv("HOME") .. "/.config/awesome/themes/spacebums-gruv"
theme.font                                      = "JetBrainsMono 14"
theme.font_nosize                               = "JetBrainsMono" -- required for some widgets that only need the theme font name.
theme.fg_normal                                 = aqua -- Wibar text colour
theme.fg_focus                                  = yellow -- App text colour
theme.fg_urgent                                 = red
theme.bg_normal                                 = bg0_h
theme.bg_focus                                  = bg0
theme.bg_urgent                                 = bg4
theme.border_normal                             = bg0
theme.border_focus                              = orange
theme.border_marked                             = green
theme.tasklist_bg_focus                         = bg0
theme.wibox_bg_color                            = bg0_h
theme.border_width                              = dpi(4)
theme.titlebar_bg_normal                        = theme.bg_normal
theme.titlebar_bg_focus                         = theme.bg_focus
theme.titlebar_fg_focus                         = theme.fg_focus
theme.menu_height                               = dpi(22)
theme.menu_width                                = dpi(200)
theme.menu_submenu_icon                         = theme.dir .. "/icons/submenu.png"
theme.taglist_squares_sel                       = theme.dir .. "/icons/square_sel.png"
theme.taglist_squares_unsel                     = theme.dir .. "/icons/square_unsel.png"
theme.layout_tile                               = theme.dir .. "/icons/tile.png"
theme.layout_tileleft                           = theme.dir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.dir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.dir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.dir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.dir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.dir .. "/icons/spiral.png"
theme.layout_centerwork                         = theme.dir .. "/icons/centerwork.png"
theme.layout_dwindle                            = theme.dir .. "/icons/dwindle.png"
theme.layout_max                                = theme.dir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.dir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.dir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.dir .. "/icons/floating.png"
theme.widget_ac                                 = theme.dir .. "/icons/ac.png"
theme.widget_battery                            = theme.dir .. "/icons/battery.png"
theme.widget_battery_low                        = theme.dir .. "/icons/battery_low.png"
theme.widget_battery_empty                      = theme.dir .. "/icons/battery_empty.png"
theme.widget_mem                                = theme.dir .. "/icons/mem.png"
theme.widget_cpu                                = theme.dir .. "/icons/cpu.png"
theme.widget_temp                               = theme.dir .. "/icons/temp.png"
theme.widget_net                                = theme.dir .. "/icons/net.png"
theme.widget_hdd                                = theme.dir .. "/icons/hdd.png"
theme.widget_music                              = theme.dir .. "/icons/note.png"
theme.widget_music_on                           = theme.dir .. "/icons/note_on.png"
theme.widget_vol                                = theme.dir .. "/icons/vol.png"
theme.widget_vol_low                            = theme.dir .. "/icons/vol_low.png"
theme.widget_vol_no                             = theme.dir .. "/icons/vol_no.png"
theme.widget_vol_mute                           = theme.dir .. "/icons/vol_mute.png"
theme.widget_mail                               = theme.dir .. "/icons/mail.png"
theme.widget_mail_on                            = theme.dir .. "/icons/mail_on.png"
theme.tasklist_plain_task_name                  = false
theme.tasklist_disable_task_name                = false
theme.tasklist_disable_icon                     = false
theme.useless_gap                               = dpi(3)  -- set to 3 for gaps on
theme.titlebar_close_button_focus               = theme.dir .. "/icons/titlebar/close_focus.png"
theme.titlebar_close_button_normal              = theme.dir .. "/icons/titlebar/close_normal.png"
theme.titlebar_ontop_button_focus_active        = theme.dir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active       = theme.dir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive      = theme.dir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive     = theme.dir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_sticky_button_focus_active       = theme.dir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active      = theme.dir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive     = theme.dir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive    = theme.dir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_floating_button_focus_active     = theme.dir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active    = theme.dir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive   = theme.dir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive  = theme.dir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_maximized_button_focus_active    = theme.dir .. "/icons/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active   = theme.dir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme.dir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme.dir .. "/icons/titlebar/maximized_normal_inactive.png"

-- Logout Popup Widget
local logout_popup = require("awesome-wm-widgets.logout-popup-widget.logout-popup")

-- MPD
local musicplr = awful.util.terminal .. " -title Music -g 130x34-320+16 -e ncmpcpp"
local mpdicon = wibox.widget.imagebox(theme.widget_music)
 mpdicon:buttons(my_table.join(
     awful.button({ modkey }, 1, function () awful.spawn.with_shell(musicplr) end),
     awful.button({ }, 1, function ()
         os.execute("mpc prev")
         theme.mpd.update()
     end),
     awful.button({ }, 2, function ()
         os.execute("mpc toggle")
         theme.mpd.update()
     end),
     awful.button({ }, 3, function ()
         os.execute("mpc next")
         theme.mpd.update()
     end)))
theme.mpd = lain.widget.mpd({
    settings = function()
        if mpd_now.state == "play" then
            artist = "󰐊 " .. mpd_now.artist .. ": "
            title  = mpd_now.title  .. " "
            mpdicon:set_image(theme.widget_music_on)
        elseif mpd_now.state == "pause" then
            artist = " "
            title  = "󰏤"
        else
            artist = ""
            title  = ""
            mpdicon:set_image(theme.widget_music)
        end

        widget:set_markup(markup.font(theme.font, markup(gray2, artist) .. title))
    end
})

-- MEM
--local memicon = wibox.widget.imagebox(theme.widget_mem) --(Disabled as we are using a font glyph)
local mem = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.font(theme.font, markup(purple, "  " .. mem_now.used .. " MB")))
    end
})

-- CPU
--local cpuicon = wibox.widget.imagebox(theme.widget_cpu) --(Disabled as we are using a font glyph)
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.font(theme.font, markup(blue, "  " .. cpu_now.usage .. "%")))
    end
})

-- CORETEMP:  Intel ONLY CPU (uses lm_sensors)
--local tempicon = wibox.widget.imagebox(theme.widget_temp)
local temp = awful.widget.watch({awful.util.shell, '-c', 'sensors | rg Package'}, 6,
function(widget, stdout)
    local temps = ""
    for line in stdout:gmatch("[^\r\n]+") do
      temps = " " .. temps .. line:match("+(%d+).*°C") .. "° "
    end
    widget:set_markup(markup.font(theme.font, markup(yellow, "" .. temps)))
end)

-- CORETEMP: AMD : If you have a AMD CPU (comment out the Intel code above if you enable this.)
-- local tempicon = wibox.widget.imagebox(theme.widget_temp)
-- local temp = lain.widget.temp({
--     settings = function()
--         widget:set_markup(markup.font(theme.font, " " .. coretemp_now .. "°C "))
--     end
-- })

-- Battery
-- local baticon = wibox.widget.imagebox(theme.widget_battery)
-- local bat = lain.widget.bat({
--     settings = function()
--         if bat_now.status and bat_now.status ~= "N/A" then
--             if bat_now.ac_status == 1 then
--                 baticon:set_image(theme.widget_ac)
--             elseif not bat_now.perc and tonumber(bat_now.perc) <= 5 then
--                 baticon:set_image(theme.widget_battery_empty)
--             elseif not bat_now.perc and tonumber(bat_now.perc) <= 15 then
--                 baticon:set_image(theme.widget_battery_low)
--             else
--                 baticon:set_image(theme.widget_battery)
--             end
--             widget:set_markup(markup.font(theme.font, " " .. bat_now.perc .. "% "))
--         else
--             widget:set_markup(markup.font(theme.font, " AC "))
--             baticon:set_image(theme.widget_ac)
--         end
--     end
-- })


-- -- Net
-- local neticon = wibox.widget.imagebox(theme.widget_net)
-- local net = lain.widget.net({
--     settings = function()
--         widget:set_markup(markup.font(theme.font,
--                           markup("#7AC82E", "󰲝 " .. string.format("%06.1f", net_now.received))
--                           .. " " ..
--                           markup("#46A8C3", " : " .. string.format("%06.1f", net_now.sent) .. " ")))
--     end
-- })

-- Weather Widget
local weather_widget = require("awesome-wm-widgets.weather-widget.weather")
local weather = weather_widget({
-- (Sign up and use your own api_key from https://home.openweathermap.org/api_keys)
     api_key='dce4b94f5a081b287f5ea48d4f13e182', -- edit this
     coordinates = {53.467, -3.017}, -- edit this (search for your city here https://openweathermap.org/city/2655198 your long/lat will show in search)
     font_name = theme.font_nosize,
     time_format_12h = true,
     units = 'metric',
     both_units_widget = false,
     icons = 'VitalyGorbachev',
     icons_extension = '.svg',
     show_hourly_forecast = true,
     show_daily_forecast = true,
})

-- Calendar
local calendar_widget = require("awesome-wm-widgets.calendar-widget.calendar")
-- Create a textclock widget
local clock = awful.widget.watch(
    "date +'%a %d %b %R'", 60,
    function(widget, stdout)
        widget:set_markup(" " .. markup.fontfg(theme.font, fg4, stdout))
    end)
local cw = calendar_widget({
    theme = 'outrun',
    placement = 'top_right',
    radius = 15,
})
clock:connect_signal("button::press",
    function(_, _, _, button)
        if button == 1 then cw.toggle() end
    end)

-- Separators
local spr     = wibox.widget.textbox(markup.fontfg(theme.font, "#555555", " " ))
local sprD     = wibox.widget.textbox(markup.fontfg(theme.font, "#555555", "  " ))
local arrl_dl = separators.arrow_left(theme.bg_focus, "alpha")
local arrl_ld = separators.arrow_left("alpha", theme.bg_focus)

-- Todo widgit
--local todo_widget = require("awesome-wm-widgets.todo-widget.todo")

function theme.at_screen_connect(s)
    -- Quake application
    s.quake = lain.util.quake({ app = awful.util.terminal })

    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.layouts)

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(my_table.join(
                           awful.button({}, 1, function () awful.layout.inc( 1) end),
                           awful.button({}, 2, function () awful.layout.set( awful.layout.layouts[1] ) end),
                           awful.button({}, 3, function () awful.layout.inc(-1) end),
                           awful.button({}, 4, function () awful.layout.inc( 1) end),
                           awful.button({}, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = dpi(22), bg = theme.wibox_bg_color, fg = theme.fg_normal })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.container.background(s.mylayoutbox),
            spr,
            s.mytaglist,
            s.mypromptbox,
            spr,
        },
    s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            spr,
            wibox.container.background(mpdicon),
            wibox.container.background(theme.mpd.widget),
            spr,
            --neticon,
            net,
            --spr,
            --memicon,
            mem,
            spr,
            --cpuicon,
            cpu,
            spr,
            --tempicon,
            temp,
            --sprD,
            --todo_widget(),
            s.systray,
            wibox.widget.systray(),
            spr,
            weather,
            --spr,
            --baticon,
            --bat,
            --spr,
            clock,
            spr,
            logout_popup.widget{},
        },
    }
end

return theme

